/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2002 Richard Hult <rhult@codefactory.se>
 * Copyright (C) 2002 Tim Waugh <twaugh@redhat.com>
 * Copyright (C) 2004 Red Hat, Inc.
 * Rewritten by Colin Walters <walters@redhat.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __EC_TRAY_ICON__
#define __EC_TRAY_ICON__

#include "eggtrayicon.h"
#include "config.h"

G_BEGIN_DECLS

#define EC_TYPE_TRAY_ICON         (ec_tray_icon_get_type ())
#define EC_TRAY_ICON(o)           (G_TYPE_CHECK_INSTANCE_CAST ((o), EC_TYPE_TRAY_ICON, ECTrayIcon))
#define EC_TRAY_ICON_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST((k), EC_TYPE_TRAY_ICON, ECTrayIconClass))
#define EC_IS_TRAY_ICON(o)        (G_TYPE_CHECK_INSTANCE_TYPE ((o), EC_TYPE_TRAY_ICON))
#define EC_IS_TRAY_ICON_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), EC_TYPE_TRAY_ICON))
#define EC_TRAY_ICON_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), EC_TYPE_TRAY_ICON, ECTrayIconClass)) 

typedef struct ECTrayIconPrivate ECTrayIconPrivate;

typedef struct
{
	EggTrayIcon parent;

	ECTrayIconPrivate *priv;
} ECTrayIcon;

typedef struct
{
	EggTrayIconClass parent;
} ECTrayIconClass;

GType       ec_tray_icon_get_type (void);

ECTrayIcon *ec_tray_icon_new (void);

void        ec_tray_icon_show_about_window (void);


G_END_DECLS

#endif /* __EC_TRAY_ICON_H__ */
