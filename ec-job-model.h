/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2004 Red Hat, Inc.
 * Written by Colin Walters <walters@redhat.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __EC_JOB_MODEL_H__
#define __EC_JOB_MODEL_H__

#include "config.h"
#include <glib-object.h>
#include <libgnomecups/gnome-cups-queue.h>
#include <gtk/gtkliststore.h>

G_BEGIN_DECLS

typedef enum {
	EC_JOB_STATE_SUBMITTED,
	EC_JOB_STATE_SUBMITTED_UNKNOWN,
	EC_JOB_STATE_LOCAL,
	EC_JOB_STATE_LOCAL_UNKNOWN,
	EC_JOB_STATE_REMOTE,
	EC_JOB_STATE_REMOTE_UNKNOWN,
	EC_JOB_STATE_FINAL,
	EC_JOB_STATE_FINAL_TIMEOUT,
} ECJobState;

GType ec_job_state_get_type (void);
#define EC_TYPE_JOB_STATE (ec_job_state_get_type ())

enum
{
	EC_JOB_MODEL_COLUMN_STATE = 0,
	EC_JOB_MODEL_COLUMN_PENDING_STATE,
	EC_JOB_MODEL_COLUMN_LOCAL_JOBID,
	EC_JOB_MODEL_COLUMN_REMOTE_JOBID,
	EC_JOB_MODEL_COLUMN_PRINTER,
	EC_JOB_MODEL_COLUMN_LOCAL_SUBMISSION_TIME,
	EC_JOB_MODEL_COLUMN_LOCAL_SUBMISSION_TIME_RELATIVE,
	EC_JOB_MODEL_COLUMN_REMOTE_SUBMISSION_TIME,
	EC_JOB_MODEL_COLUMN_REMOTE_SUBMISSION_TIME_RELATIVE,
	EC_JOB_MODEL_COLUMN_JOB,
	EC_JOB_MODEL_NUM_COLUMNS /* Should be last */
};

#define EC_TYPE_JOB_MODEL (ec_job_model_get_type ())
#define EC_JOB_MODEL(o) (G_TYPE_CHECK_INSTANCE_CAST ((o), EC_TYPE_JOB_MODEL, ECJobModel))
#define EC_JOB_MODEL_CLASS(k) (G_TYPE_CHECK_CLASS_CAST((k), EC_TYPE_JOB_MODEL, ECJobModelClass))
#define EC_IS_JOB_MODEL(o) (G_TYPE_CHECK_INSTANCE_TYPE ((o), EC_TYPE_JOB_MODEL))
#define EC_IS_JOB_MODEL_CLASS(k) (G_TYPE_CHECK_CLASS_TYPE ((k), EC_TYPE_JOB_MODEL))
#define EC_JOB_MODEL_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), EC_TYPE_JOB_MODEL, ECJobModelClass)) 

typedef struct ECJobModelPrivate ECJobModelPrivate;

typedef struct
{
	GtkListStore parent;

	ECJobModelPrivate *priv;
} ECJobModel;

typedef struct
{
	GtkListStoreClass parent;

	void (*state_changed) (ECJobModel   *model,
			       gchar        *printer,
			       gint         *local_job_id,
			       gpointer      job);
} ECJobModelClass;

GType ec_job_model_get_type (void);

ECJobModel * ec_job_model_new (void);

void ec_job_model_job_submitted_local (ECJobModel *list,
				       const char *printer_name,
				       guint32 job_id);

void ec_job_model_job_started_local (ECJobModel *list,
				     const char *printer_name,
				     guint32 job_id);

void ec_job_model_job_sent_remote (ECJobModel *list,
				   const char *printer_name,
				   guint32 local_job_id,
				   const char *job_uri,
				   guint32 remote_job_id);

void ec_job_model_remove_local_job (ECJobModel *model,
				    guint32 job_id);

void ec_job_model_remove_remote_job (ECJobModel *model,
				     guint32 job_id);

gint ec_job_model_job_get_pending_state (ECJobModel *model,
					 guint32     local_job_id);
void ec_job_model_job_set_pending_state (ECJobModel *model,
					 guint32     local_job_id,
					 gint        pending_state);

void ec_job_model_remove_old_completed (ECJobModel *model,
					long delta);

G_END_DECLS

#endif /* __EC_JOB_MODEL_H__ */
