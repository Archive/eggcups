# translation of eggcups.HEAD.po to Punjabi
# translation of pa.po to Punjabi
# This file is distributed under the same license as the PACKAGE package.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER.
# Amanpreet Singh Alam <aalam@redhat.com>, 2004.
# Amanpreet Singh Alam <amanpreetalam@yahoo.com>, 2005.
#
msgid ""
msgstr ""
"Project-Id-Version: eggcups.HEAD\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2005-07-10 13:19+0000\n"
"PO-Revision-Date: 2005-07-10 23:56+0530\n"
"Last-Translator: Amanpreet Singh Alam <amanpreetalam@yahoo.com>\n"
"Language-Team: Punjabi <fedora-transa-pa@redhat.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: Plural-Forms: Plural-Forms: nplurals=2; plural=(n != 1);\n"
"\n"
"X-Generator: KBabel 1.9.1\n"

#: ../ec-cups-job-monitor.c:234 ../ec-cups-job-monitor.c:235
#: ../ec-cups-job-monitor.c:236 ../ec-cups-job-monitor.c:252
#: ../ec-cups-job-monitor.c:255 ../ec-job-list.c:889 ../ec-job-list.c:909
#: ../ec-job-list.c:910 ../ec-job-list.c:931 ../ec-job-list.c:949
#: ../ec-job-list.c:950 ../ec-job-list.c:1029 ../ec-job-list.c:1111
#: ../ec-job-list.c:1166 ../ec-job-list.c:1170 ../ec-job-list.c:1185
msgid "Unknown"
msgstr "ਅਣਜਾਣ"

#: ../ec-cups-job-monitor.c:237
msgid "Pending"
msgstr "ਬਕਾਇਆ"

#: ../ec-cups-job-monitor.c:238
msgid "Paused"
msgstr "ਵਿਰਾਮ"

#: ../ec-cups-job-monitor.c:239
msgid "Printing"
msgstr "ਛਾਪ ਰਹੇ"

#: ../ec-cups-job-monitor.c:240
msgid "Stopped"
msgstr "ਰੁਕਿਆ"

#: ../ec-cups-job-monitor.c:241
msgid "Cancelled"
msgstr "ਰੱਦ ਕੀਤੇ"

#: ../ec-cups-job-monitor.c:242
msgid "Aborted"
msgstr "ਅਧੂਰੇ ਛੱਡੇ"

#: ../ec-cups-job-monitor.c:243
msgid "Completed"
msgstr "ਸਮਾਪਤ"

#: ../ec-job-list.c:268
msgid "Document List"
msgstr "ਦਸਤਾਵੇਜ਼ ਸੂਚੀ"

#: ../ec-job-list.c:270
msgid "Lists the user's documents submitted for printing"
msgstr "ਛਪਾਈ ਲਈ ਉਪਭੋਗੀਆਂ ਦੇ ਦਸਤਾਵੇਜ਼ਾਂ ਦੀ ਸੂਚੀ"

#: ../ec-job-list.c:280
msgid "Document"
msgstr "ਦਸਤਾਵੇਜ਼"

#: ../ec-job-list.c:291 ../gnome-default-printer.c:254
msgid "Printer"
msgstr "ਪ੍ਰਿੰਟਰ"

#: ../ec-job-list.c:303
msgid "Size"
msgstr "ਅਕਾਰ"

#: ../ec-job-list.c:315
msgid "Time Submitted"
msgstr "ਭੇਜਣ ਦਾ ਸਮਾਂ"

#: ../ec-job-list.c:328
msgid "Position"
msgstr "ਸਥਿਤੀ"

#: ../ec-job-list.c:340
msgid "Status"
msgstr "ਹਾਲਤ"

#: ../ec-job-list.c:361
msgid "Document print status"
msgstr "ਦਸਤਾਵੇਜ਼ ਛਾਪਣ ਸਥਿਤੀ"

#: ../ec-job-list.c:476
#, c-format
msgid "Cancel printing of \"%s\" on printer \"%s\"?"
msgstr "\"%s\" ਦਾ ਪ੍ਰਿੰਟਰ \"%s\" ਤੇ ਛਾਪਣਾ ਰੱਦ ਕਰਨਾ ਹੈ?"

#: ../ec-job-list.c:482
msgid "If you select \"Cancel printing\", the selected job will be cancelled."
msgstr "ਜੇਕਰ ਤੁਸੀਂ \"ਛਪਾਈ ਰੱਦ ਕਰੋ\" ਦੀ ਚੋਣ ਕੀਤੀ ਤਾਂ ਚੁਣੇ ਕੰਮਾਂ ਨੂੰ ਰੱਦ ਕਰ ਦਿੱਤਾ ਜਾਵੇਗਾ।"

#: ../ec-job-list.c:485 ../ec-job-list.c:508
msgid "Keep printing"
msgstr "ਛਾਪਣਾ ਜਾਰੀ ਰੱਖੋ"

#: ../ec-job-list.c:487 ../ec-job-list.c:510
msgid "Cancel printing"
msgstr "ਛਾਪਣਾ ਰੱਦ"

#: ../ec-job-list.c:493
#, c-format
msgid "Cancel printing of %d document?"
msgid_plural "Cancel printing of %d documents?"
msgstr[0] "ਕੀ %d ਦਸਤਾਵੇਜ਼ ਰੱਦ ਕਰਨਾ ਹੈ?"
msgstr[1] "ਕੀ %d ਦਸਤਾਵੇਜ਼ ਰੱਦ ਕਰਨੇ ਹਨ?"

#: ../ec-job-list.c:505
msgid "If you select \"Cancel printing\", the selected jobs will be cancelled."
msgstr "ਜੇਕਰ ਤੁਸੀਂ \"ਛਪਾਈ ਰੱਦ ਕਰੋ\" ਦੀ ਚੋਣ ਕੀਤੀ ਤਾਂ ਚੁਣੇ ਕੰਮਾਂ ਨੂੰ ਰੱਦ ਕਰ ਦਿੱਤਾ ਜਾਵੇਗਾ।"

#: ../ec-job-list.c:690 ../ec-job-list.c:798
msgid "_Cancel Documents"
msgstr "ਦਸਤਾਵੇਜ਼ ਰੱਦ(_C)"

#: ../ec-job-list.c:697 ../ec-job-list.c:810
msgid "_Pause Documents"
msgstr "ਦਸਤਾਵੇਜ਼ ਵਿਰਾਮ(_P)"

#: ../ec-job-list.c:704 ../ec-job-list.c:822
msgid "_Resume Documents"
msgstr "ਦਸਤਾਵੇਜ਼ ਮੁੜ-ਚਾਲੂ(_R)"

#. File menu.
#: ../ec-job-list.c:772
msgid "_File"
msgstr "ਫਾਇਲ(_F)"

#: ../ec-job-list.c:783
msgid "Close the window"
msgstr "ਝਰੋਖਾ ਬੰਦ ਕਰੋ"

#. Edit menu.
#: ../ec-job-list.c:791
msgid "_Edit"
msgstr "ਸੋਧ(_E)"

#: ../ec-job-list.c:802
msgid "Cancel printing of the selected documents"
msgstr "ਚੁਣੇ ਦਸਤਾਵੇਜ਼ਾਂ ਦੀ ਛਪਾਈ ਰੱਦ ਕਰੋ"

#: ../ec-job-list.c:814
msgid "Pause printing of the selected documents"
msgstr "ਚੁਣੇ ਦਸਤਾਵੇਜ਼ਾਂ ਲਈ ਛਪਾਈ ਨੂੰ ਵਿਰਾਮ"

#: ../ec-job-list.c:826
msgid "Resume printing of the selected documents"
msgstr "ਚੁਣੇ ਦਸਤਾਵੇਜ਼ਾਂ ਲਈ ਛਪਾਈ ਮੁੜ-ਚਾਲੂ"

#: ../ec-job-list.c:834
msgid "C_lear Completed Documents"
msgstr "ਮੁਕੰਮਲ ਦਸਤਾਵੇਜ਼ ਸਾਫ਼ ਕਰੋ(_l)"

#: ../ec-job-list.c:838
msgid "Remove completed documents from the list"
msgstr "ਸੂਚੀ 'ਚੋਂ ਮੁਕਮੰਲ ਦਸਤਾਵੇਜ਼ ਹਟਾਓ"

#. Help menu.
#: ../ec-job-list.c:849
msgid "_Help"
msgstr "ਸਹਾਇਤਾ(_H)"

#: ../ec-job-list.c:1038
#, c-format
msgid "%ld minute"
msgid_plural "%ld minutes"
msgstr[0] "%ld ਮਿੰਟ"
msgstr[1] "%ld ਮਿੰਟ"

#: ../ec-job-list.c:1039
#, c-format
msgid "%ld hour"
msgid_plural "%ld hours"
msgstr[0] "%ld ਘੰਟਾ"
msgstr[1] "%ld ਘੰਟੇ"

#: ../ec-job-list.c:1040
#, c-format
msgid "%ld day"
msgid_plural "%ld days"
msgstr[0] "%ld ਦਿਨ"
msgstr[1] "%ld ਦਿਨ"

#. Translators: the format is "X days, X hours and X minutes ago"
#: ../ec-job-list.c:1044
#, c-format
msgid "%s, %s ago"
msgstr "%s, %s ਪਹਿਲਾਂ"

#. Translators: the format is "X hours and X minutes ago"
#: ../ec-job-list.c:1050
#, c-format
msgid "%s and %s ago"
msgstr "%s ਅਤੇ %s ਪਹਿਲਾਂ"

#. Translators: the format is "X minutes ago"
#: ../ec-job-list.c:1056
#, c-format
msgid "%s ago"
msgstr "%s ਪਹਿਲਾਂ"

#: ../ec-job-list.c:1060
msgid "Less than one minute ago"
msgstr "ਇੱਕ ਮਿੰਟ ਤੋਂ ਪਹਿਲਾਂ"

#: ../ec-job-list.c:1176
msgid "Sending"
msgstr "ਭੇਜਿਆ ਜਾਦਾ ਹੈ"

#: ../ec-job-list.c:1182
msgid "Sending (?)"
msgstr "ਭੇਜਿਆ ਜਾ ਰਿਹਾ ਹੈ (?)"

#: ../ec-tray-icon.c:99
msgid "/_About"
msgstr "/ਇਸ ਬਾਰੇ(_A)"

#: ../ec-tray-icon.c:102
msgid "/_Hide"
msgstr "/ਓਹਲੇ(_H)"

#: ../ec-tray-icon.c:363
msgid "About Print Notifier"
msgstr "ਛਪਾਈ ਸੂਚਨਾ ਬਾਰੇ"

#: ../ec-tray-icon.c:385 ../main.c:123
msgid "Print Notifier"
msgstr "ਪ੍ਰਿੰਟਰ ਸੂਚਨਾ"

#: ../ec-tray-icon.c:386
msgid "A program for print job notification."
msgstr "ਛਪਾਈ ਕੰਮ ਸੂਚਨਾ ਲਈ ਪਰੋਗਰਾਮ ਹੈ।"

#: ../ec-tray-icon.c:390
msgid "Based on code by CodeFactory."
msgstr "ਕੋਡੀਫੈਕਟਰੀ ਦੇ ਕੋਡ ਦੇ ਆਧਾਰ 'ਤੇ"

#: ../ec-tray-icon.c:641
#, c-format
msgid "Password for printer \"%s\""
msgstr "ਪ੍ਰਿੰਟਰ \"%s\" ਲਈ ਗੁਪਤ-ਕੋਡ"

#: ../ec-tray-icon.c:665
msgid "Couldn't store password in keyring"
msgstr "ਕੀਰਿੰਗ 'ਚ ਗੁਪਤ-ਕੋਡ ਸੰਭਾਲਿਆ ਨਹੀਂ ਜਾ ਸਕਦਾ ਹੈ"

#: ../ec-tray-icon.c:667
#, c-format
msgid "Got error code %d while trying to store password"
msgstr "ਸੰਭਾਲੇ ਗੁਪਤ-ਕੋਡ ਨਾਲ ਕੋਸ਼ਿਸ ਕਰਨ 'ਤੇ ਗਲਤੀ ਕੋਡ %d ਮਿਲਿਆ"

#: ../ec-tray-icon.c:704
#, c-format
msgid "Enter password for printer \"%s\""
msgstr "ਪ੍ਰਿੰਟਰ \"%s\" ਲਈ ਗੁਪਤ-ਕੋਡ ਦਿਓ"

#: ../ec-tray-icon.c:1000
#, c-format
msgid "%d documents queued"
msgstr "%d ਦਸਤਾਵੇਜ਼ ਕਤਾਰ ਵਿੱਚ ਹਨ"

#: ../ec-tray-icon.c:1008
msgid "No documents queued for printing"
msgstr "ਛਪਾਈ ਲਈ ਕੋਈ ਦਸਤਾਵੇਜ਼ ਨਹੀਂ"

#: ../ec-tray-icon.c:1117
msgid ""
"At least one of the resources needed by the document, such as media, fonts, "
"resource objects, etc., is not ready."
msgstr ""
"ਦਸਤਾਵੇਜ਼ ਰਾਹੀਂ ਲੋੜੀਦਾ ਘੱਟੋ-ਘੱਟ ਇੱਕ ਸਰੋਤ, ਜਿਵੇਂ ਕਿ ਮੀਡਿਆ, ਫੋਂਟ, ਸਰੋਤ ਆਬਜੈਕਟ, ਆਦਿ "
"ਤਿਆਰ ਨਹੀਂ ਹੈ।"

#: ../ec-tray-icon.c:1121
msgid "The printer is partly stopped."
msgstr "ਪ੍ਰਿੰਟਰ ਅਧੂਰਾ ਰੁਕਿਆ ਹੈ।"

#: ../ec-tray-icon.c:1123
msgid "The printer is stopped."
msgstr "ਪ੍ਰਿੰਟਰ ਰੁਕਿਆ ਹੈ।"

#: ../ec-tray-icon.c:1125
msgid "Someone else has paused it."
msgstr "ਕਿਸੇ ਹੋਰ ਨੇ ਵਿਰਾਮ ਕਰ ਦਿੱਤਾ ਹੈ।"

#: ../ec-tray-icon.c:1127 ../ec-tray-icon.c:1154 ../ec-tray-icon.c:1183
msgid "You may want to find out why."
msgstr "ਤੁਸੀਂ ਜਾਣਨਾ ਚਾਹੁੰਦੇ ਹੋਵੇਗੇ ਕਿ ਕਿਉ।"

#: ../ec-tray-icon.c:1133
#, c-format
msgid "Printing of \"%s\" on printer \"%s\" was paused."
msgstr "\"%s\" ਦੀ ਛਪਾਈ ਪ੍ਰਿੰਟਰ \"%s\" ਉੱਤੇ ਵਿਰਾਮ ਹੋਈ ਹੈ।"

#: ../ec-tray-icon.c:1150
msgid "An operator has cancelled it."
msgstr "ਇੱਕ ਓਪਰੇਟਰ ਨੇ ਇਸ ਰੱਦ ਕਰ ਦਿੱਤਾ ਹੈ।"

#: ../ec-tray-icon.c:1152
msgid "Someone at the printer has cancelled it."
msgstr "ਪ੍ਰਿੰਟਰ ਉੱਤੇ ਕਿਸੇ ਨੇ ਰੱਦ ਕਰ ਦਿੱਤਾ ਹੈ।"

#: ../ec-tray-icon.c:1160
#, c-format
msgid "Printing of \"%s\" on printer \"%s\" was cancelled."
msgstr "\"%s\" ਦੀ ਛਪਾਈ ਪ੍ਰਿੰਟਰ \"%s\" ਉੱਤੇ ਰੱਦ ਕਰ ਦਿੱਤੀ ਗਈ ਸੀ।"

#: ../ec-tray-icon.c:1173
msgid ""
"The document was aborted by the system because the printer could not "
"decompress the data."
msgstr "ਸਿਸਟਮ ਨੇ ਦਸਤਾਵੇਜ਼ ਨੂੰ ਅਧੂਰਾ ਛੱਡ ਦਿੱਤਾ ਹੈ, ਕਿਉਕਿ ਪ੍ਰਿੰਟਰ ਡਾਟੇ ਨੂੰ ਖੋਲ ਨਹੀਂ ਸਕਦਾ ਹੈ।"

#: ../ec-tray-icon.c:1179
msgid ""
"The document was aborted by the system because the printer did not "
"understand the document format."
msgstr ""
"ਸਿਸਟਮ ਨੇ ਦਸਤਾਵੇਜ਼ ਨੂੰ ਅਧੂਰਾ ਛੱਡ ਦਿੱਤਾ ਹੈ, ਕਿਉਕਿ ਪ੍ਰਿੰਟਰ ਦਸਤਾਵੇਜ਼ ਫਾਰਮਿਟ ਨੂੰ ਸਮਝ "
"ਨਹੀਂ ਸਕਿਆ ਹੈ।"

#: ../ec-tray-icon.c:1189
#, c-format
msgid "Printing of \"%s\" on printer \"%s\" was aborted."
msgstr "\"%s\" ਦੀ ਛਪਾਈ ਨੂੰ \"%s\" ਪ੍ਰਿੰਟਰ ਉੱਤੇ ਅਧੂਰਾ ਛੱਡਿਆ ਗਿਆ ਹੈ।"

#: ../eggtrayicon.c:118
msgid "Orientation"
msgstr "ਸਥਿਤੀ"

#: ../eggtrayicon.c:119
msgid "The orientation of the tray."
msgstr "ਟਰੇ ਦੀ ਸਥਿਤੀ ਹੈ।"

#: ../gnome-default-printer.c:231 ../gnome-default-printer.desktop.in.h:1
msgid "Default Printer"
msgstr "ਮੂਲ ਪ੍ਰਿੰਟਰ"

#: ../gnome-default-printer.c:236
msgid "_Set Default"
msgstr "ਮੂਲ ਸੈਟ ਕਰੋ(_S)"

#: ../gnome-default-printer.c:262
msgid "Location"
msgstr "ਟਿਕਾਣਾ"

#: ../gnome-default-printer.desktop.in.h:2
msgid "Select default printer"
msgstr "ਮੂਲ ਪ੍ਰਿੰਟਰ ਚੁਣੋ"

#: ../main.c:111
msgid "Enable debugging information"
msgstr "ਡੀਬੱਗ ਜਾਣਕਾਰੀ ਯੋਗ"

#: ../main.c:112
msgid "Show icon by default"
msgstr "ਮੂਲ ਰੂਪ 'ਚ ਆਈਕਾਨ ਵੇਖਾਓ"

#: ../driverdialog.glade.h:2
#, no-c-format
msgid ""
"<span weight=\"bold\" size=\"larger\">Specify Printer Driver</span>\n"
"\n"
"You have just plugged in a '%s %s' printer. The proper driver for the "
"printer was not detected. Please select a driver:"
msgstr ""
"<span weight=\"bold\" size=\"larger\">ਪ੍ਰਿੰਟਰ ਡਰਾਇਵਰ ਦਿਓ</span>\n"
"\n"
"ਤੁਸੀਂ ਇੱਕ '%s %s' ਪ੍ਰਿੰਟਰ ਪਲੱਗ ਕੀਤਾ ਹੈ। ਪ੍ਰਿੰਟਰ ਲਈ ਠੀਕ ਡਰਾਇਵਰ ਖੋਜਿਆ ਨਹੀ "
"ਜਾ ਸਕਿਆ ਹੈ। ਕਿਰਪਾ ਕਰਕੇ ਇੱਕ ਡਰਾਇਵਰ ਦੀ ਚੋਣ ਕਰੋ:"

#: ../driverdialog.glade.h:5
msgid "Manufacturer:"
msgstr "ਨਿਰਮਾਤਾ:"

#: ../driverdialog.glade.h:6
msgid "Model:"
msgstr "ਮਾਡਲ:"

