/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2004 Red Hat, Inc.
 * Written by Colin Walters <walters@redhat.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __EC_CUPS_JOB_MONITOR__
#define __EC_CUPS_JOB_MONITOR_H__

#include "config.h"
#include <glib-object.h>
#include <libgnomecups/gnome-cups-queue.h>

G_BEGIN_DECLS

#define EC_TYPE_CUPS_JOB_MONITOR (ec_cups_job_monitor_get_type ())
#define EC_CUPS_JOB_MONITOR(o) (G_TYPE_CHECK_INSTANCE_CAST ((o), EC_TYPE_CUPS_JOB_MONITOR, ECCupsJobMonitor))
#define EC_CUPS_JOB_MONITOR_CLASS(k) (G_TYPE_CHECK_CLASS_CAST((k), EC_TYPE_CUPS_JOB_MONITOR, ECCupsJobMonitorClass))
#define EC_IS_CUPS_JOB_MONITOR(o) (G_TYPE_CHECK_INSTANCE_TYPE ((o), EC_TYPE_CUPS_JOB_MONITOR))
#define EC_IS_CUPS_JOB_MONITOR_CLASS(k) (G_TYPE_CHECK_CLASS_TYPE ((k), EC_TYPE_CUPS_JOB_MONITOR))
#define EC_CUPS_JOB_MONITOR_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), EC_TYPE_CUPS_JOB_MONITOR, ECCupsJobMonitorClass)) 

typedef struct ECCupsJobMonitorPrivate ECCupsJobMonitorPrivate;

typedef struct
{
	GObject parent;

	ECCupsJobMonitorPrivate *priv;
} ECCupsJobMonitor;

typedef struct
{
	GObjectClass parent;

	void (*job_changed) (ECCupsJobMonitor *mon,
			     GnomeCupsJob *job);
	void (*job_timeout) (ECCupsJobMonitor *mon, const char *printer_name,
			     guint job_id, GTimeVal *last_time);
} ECCupsJobMonitorClass;

GType ec_cups_job_monitor_get_type (void);

ECCupsJobMonitor * ec_cups_job_monitor_new (void);

GnomeCupsJob * ec_cups_job_monitor_add_job (ECCupsJobMonitor *mon,
					    gboolean poll_now,
					    const char *host,
					    const char *printer_path,
					    guint32 job_id);

void ec_cups_job_monitor_remove_job (ECCupsJobMonitor *mon,
				     const char *host,
				     guint32 job_id);

guint ec_cups_job_monitor_get_job_count (ECCupsJobMonitor *mon);

G_END_DECLS

#endif /* __EC_CUPS_JOB_MONITOR_H__ */
