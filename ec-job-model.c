/*
 *  Copyright (C) 2004 Red Hat, Inc.
 *  Written by Colin Walters <walters@redhat.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 */

#include "ec-job-model.h"
#include "ec-cups-job-monitor.h"
#include "rb-debug.h"
#include <libgnome/gnome-i18n.h>
#include <gdk/gdk.h>
#include <gtk/gtkmarshal.h>
#include <libxml/uri.h>

#define FINAL_TIMEOUT_SECONDS (120)
#define JOB_DISAPPEARANCE_SECONDS (24 * 60 * 60)

G_DEFINE_TYPE(ECJobModel, ec_job_model, GTK_TYPE_LIST_STORE)

static void ec_job_model_finalize (GObject *object);
static void ec_job_model_dispose (GObject *object);
static GObject *ec_job_model_constructor (GType type, guint n_construct_properties,
					  GObjectConstructParam *construct_properties);

static gboolean idle_relative_time_update (ECJobModel *model);
static void job_changed_cb (ECCupsJobMonitor *mon, GnomeCupsJob *job, ECJobModel *model); 
static void job_timeout_cb (ECCupsJobMonitor *mon, const char *printer_name,
			    guint job_id, GTimeVal *last_time, ECJobModel *model); 

struct ECJobModelPrivate
{
	gboolean disposed;
	
	ECCupsJobMonitor *mon;

	guint idle_relative_time_update_id;
};

enum
{
	PROP_0,
};

enum 
{
  STATE_CHANGED,
  LAST_SIGNAL
};

static guint signals[LAST_SIGNAL];

static void
ec_job_model_class_init (ECJobModelClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	object_class->dispose = ec_job_model_dispose;
	object_class->finalize = ec_job_model_finalize;
	object_class->constructor = ec_job_model_constructor;

	signals[STATE_CHANGED] =
	  g_signal_new ("state_changed",
			G_OBJECT_CLASS_TYPE (klass),
			G_SIGNAL_RUN_LAST,
			G_STRUCT_OFFSET (ECJobModelClass, state_changed),
			NULL, NULL,
			gtk_marshal_VOID__STRING_INT_POINTER,
			G_TYPE_NONE, 3,
			G_TYPE_STRING,
			G_TYPE_INT,
			G_TYPE_POINTER);


}

static void
ec_job_model_init (ECJobModel *model)
{
	model->priv = g_new0 (ECJobModelPrivate, 1);

	model->priv->mon = ec_cups_job_monitor_new ();

	g_signal_connect_object (G_OBJECT (model->priv->mon),
				 "job-changed",
				 G_CALLBACK (job_changed_cb),
				 model, 0);
	g_signal_connect_object (G_OBJECT (model->priv->mon),
				 "job-timeout",
				 G_CALLBACK (job_timeout_cb),
				 model, 0);

	model->priv->idle_relative_time_update_id = g_timeout_add (60 * 1000,
								   (GSourceFunc) idle_relative_time_update,
								   model);
}

static void
ec_job_model_dispose (GObject *object)
{
	ECJobModel *model = EC_JOB_MODEL (object);

	if (model->priv->disposed)
		return;
	model->priv->disposed = TRUE;

	g_return_if_fail (model->priv != NULL);

	g_object_unref (G_OBJECT (model->priv->mon));
}

static void
ec_job_model_finalize (GObject *object)
{
	ECJobModel *model = EC_JOB_MODEL (object);

	g_return_if_fail (model->priv != NULL);

	if (model->priv->idle_relative_time_update_id > 0)
		g_source_remove (model->priv->idle_relative_time_update_id);

	g_free (model->priv);

	G_OBJECT_CLASS (ec_job_model_parent_class)->finalize (object);
}

static GObject *
ec_job_model_constructor (GType type, guint n_construct_properties,
			  GObjectConstructParam *construct_properties)
{
	ECJobModel *model;
	ECJobModelClass *klass;
	GObjectClass *parent_class;  
	GType *column_types;

	klass = EC_JOB_MODEL_CLASS (g_type_class_peek (EC_TYPE_JOB_MODEL));

	parent_class = G_OBJECT_CLASS (g_type_class_peek_parent (klass));
	model = EC_JOB_MODEL (parent_class->constructor (type, n_construct_properties,
							 construct_properties));

	column_types = g_new (GType, EC_JOB_MODEL_NUM_COLUMNS);
	column_types[EC_JOB_MODEL_COLUMN_STATE] = EC_TYPE_JOB_STATE;
	column_types[EC_JOB_MODEL_COLUMN_PENDING_STATE] = G_TYPE_INT;
	column_types[EC_JOB_MODEL_COLUMN_LOCAL_JOBID] = G_TYPE_UINT;
	column_types[EC_JOB_MODEL_COLUMN_REMOTE_JOBID] = G_TYPE_UINT;
	column_types[EC_JOB_MODEL_COLUMN_PRINTER] = G_TYPE_STRING;
	column_types[EC_JOB_MODEL_COLUMN_LOCAL_SUBMISSION_TIME] = G_TYPE_POINTER;
	column_types[EC_JOB_MODEL_COLUMN_LOCAL_SUBMISSION_TIME_RELATIVE] = G_TYPE_LONG;
	column_types[EC_JOB_MODEL_COLUMN_REMOTE_SUBMISSION_TIME] = G_TYPE_POINTER;
	column_types[EC_JOB_MODEL_COLUMN_REMOTE_SUBMISSION_TIME_RELATIVE] = G_TYPE_LONG;
	column_types[EC_JOB_MODEL_COLUMN_JOB] = G_TYPE_POINTER;
	gtk_list_store_set_column_types (GTK_LIST_STORE (model),
					 EC_JOB_MODEL_NUM_COLUMNS, column_types);
	g_free (column_types);

	return G_OBJECT (model);
}

ECJobModel *
ec_job_model_new (void)
{
	return EC_JOB_MODEL (g_object_new (EC_TYPE_JOB_MODEL, NULL));
}

static gboolean
ec_job_model_jobid_to_iter (ECJobModel *ecmodel, guint column, guint32 jobid,
			    GtkTreeIter *iter)
{
	GtkTreeModel *model = GTK_TREE_MODEL (ecmodel);

	if (!gtk_tree_model_get_iter_first (model, iter))
	  return FALSE;
	do {
		guint32 local_jobid;
		gtk_tree_model_get (model, iter, column, &local_jobid, -1);
		if (local_jobid == jobid)
			return TRUE;
	} while (gtk_tree_model_iter_next (model, iter));
	return FALSE;
}

static gboolean
ec_job_model_local_jobid_to_iter (ECJobModel *ecmodel, guint32 jobid, GtkTreeIter *iter)
{
	return ec_job_model_jobid_to_iter (ecmodel, EC_JOB_MODEL_COLUMN_LOCAL_JOBID, jobid, iter);
}

static gboolean
ec_job_model_remote_jobid_to_iter (ECJobModel *ecmodel, guint32 jobid, GtkTreeIter *iter)
{
	return ec_job_model_jobid_to_iter (ecmodel, EC_JOB_MODEL_COLUMN_REMOTE_JOBID, jobid, iter);
}

/* This is sort of a hack.  I don't know of a much more elegant way to
 * do it, however.
 */
static gboolean
idle_relative_time_update (ECJobModel *model)
{
	GtkTreeIter iter;
	GtkTreeModel *treemodel;
	GtkListStore *listmodel;
	GTimeVal current_time;

	GDK_THREADS_ENTER ();

	rb_debug ("relative time update");

	treemodel = GTK_TREE_MODEL (model);
	listmodel = GTK_LIST_STORE (model);

	g_get_current_time (&current_time);

	if (!gtk_tree_model_get_iter_first (treemodel, &iter))
		goto out;
	do {
		GTimeVal *local_time;
		GTimeVal *remote_time;
		ECJobState state;

		gtk_tree_model_get (treemodel, &iter,
				    EC_JOB_MODEL_COLUMN_STATE,
				    &state,
				    EC_JOB_MODEL_COLUMN_LOCAL_SUBMISSION_TIME,
				    &local_time, -1);
		if (local_time) {
			rb_debug ("updating local job time");
			gtk_list_store_set (listmodel, &iter,
					    EC_JOB_MODEL_COLUMN_LOCAL_SUBMISSION_TIME_RELATIVE,
					    (current_time.tv_sec - local_time->tv_sec) / 60, -1);
		}
		gtk_tree_model_get (treemodel, &iter,
				    EC_JOB_MODEL_COLUMN_REMOTE_SUBMISSION_TIME,
				    &remote_time, -1);
		if (remote_time) {
			rb_debug ("updating remote job time");
			gtk_list_store_set (listmodel, &iter,
					    EC_JOB_MODEL_COLUMN_REMOTE_SUBMISSION_TIME_RELATIVE,
					    (current_time.tv_sec - remote_time->tv_sec) / 60, -1);
		}
		
		if (state == EC_JOB_STATE_FINAL
		    || state == EC_JOB_STATE_FINAL_TIMEOUT) {
			gulong delta;

			if (remote_time) 
				delta = current_time.tv_sec - remote_time->tv_sec;
			else if (local_time) 
				delta = current_time.tv_sec - local_time->tv_sec;
			else
				delta = 0;
				
			if (delta >= JOB_DISAPPEARANCE_SECONDS) {
				GtkTreeIter cur_iter = iter;
				gboolean have_next;
				
				have_next = gtk_tree_model_iter_next (treemodel, &iter);
				/* This works because the list store
				 * has persistent iters.
				 */
				gtk_list_store_remove (listmodel, &cur_iter);
						       
				if (have_next)
					continue;
				else
					break;
			}
		}
	} while (gtk_tree_model_iter_next (treemodel, &iter));
	
 out:
	GDK_THREADS_LEAVE ();
	return TRUE;
}


static ECJobState
map_known_state_to_unknown (ECJobState state)
{
	switch (state)
	{
	case EC_JOB_STATE_SUBMITTED:
		return EC_JOB_STATE_SUBMITTED_UNKNOWN;
	case EC_JOB_STATE_LOCAL:
		return EC_JOB_STATE_LOCAL_UNKNOWN;
	case EC_JOB_STATE_REMOTE:
		return EC_JOB_STATE_REMOTE_UNKNOWN;
	case EC_JOB_STATE_SUBMITTED_UNKNOWN:
	case EC_JOB_STATE_LOCAL_UNKNOWN:
	case EC_JOB_STATE_REMOTE_UNKNOWN:
		return state;
	case EC_JOB_STATE_FINAL:
	case EC_JOB_STATE_FINAL_TIMEOUT:
		return state;
	}
	return EC_JOB_STATE_FINAL;
}

static gboolean
state_is_unknown (ECJobState state)
{
	switch (state)
	{
	case EC_JOB_STATE_SUBMITTED_UNKNOWN:
	case EC_JOB_STATE_LOCAL_UNKNOWN:
	case EC_JOB_STATE_REMOTE_UNKNOWN:
		return TRUE;
	case EC_JOB_STATE_SUBMITTED:
	case EC_JOB_STATE_LOCAL:
	case EC_JOB_STATE_REMOTE:
	case EC_JOB_STATE_FINAL:
	case EC_JOB_STATE_FINAL_TIMEOUT:
		break;
	}
	return FALSE;
}

static ECJobState
map_unknown_state_to_known (ECJobState state)
{
	switch (state)
	{
	case EC_JOB_STATE_SUBMITTED_UNKNOWN:
		return EC_JOB_STATE_SUBMITTED;
	case EC_JOB_STATE_LOCAL_UNKNOWN:
		return EC_JOB_STATE_LOCAL;
	case EC_JOB_STATE_REMOTE_UNKNOWN:
		return EC_JOB_STATE_REMOTE;
	case EC_JOB_STATE_SUBMITTED:
	case EC_JOB_STATE_LOCAL:
	case EC_JOB_STATE_REMOTE:
	case EC_JOB_STATE_FINAL:
	case EC_JOB_STATE_FINAL_TIMEOUT:
		g_assert_not_reached ();
	}
	return EC_JOB_STATE_FINAL;
}

static void
job_timeout_cb (ECCupsJobMonitor *mon, const char *printer_name,
		guint job_id, GTimeVal *last_time, ECJobModel *model)
{
	GTimeVal current_time;
	GtkTreeIter iter;

	rb_debug ("handling job %d timeout", job_id);

	if (!ec_job_model_remote_jobid_to_iter (model, job_id, &iter)) {
		g_assert (ec_job_model_local_jobid_to_iter (model, job_id, &iter));
	}

	g_get_current_time (&current_time);

	if ((current_time.tv_sec - last_time->tv_sec) > FINAL_TIMEOUT_SECONDS)
		gtk_list_store_set (GTK_LIST_STORE (model), &iter,
				    EC_JOB_MODEL_COLUMN_STATE, EC_JOB_STATE_FINAL_TIMEOUT,
				    -1);
	else {
		ECJobState current_state;
		gtk_tree_model_get (GTK_TREE_MODEL (model), &iter,
				    EC_JOB_MODEL_COLUMN_STATE, &current_state,
				    -1);
		gtk_list_store_set (GTK_LIST_STORE (model), &iter,
				    EC_JOB_MODEL_COLUMN_STATE, map_known_state_to_unknown (current_state),
				    -1);
	}
}

static gboolean
job_is_done (GnomeCupsJob *job)
{
	return job->state == IPP_JOB_COMPLETED
		|| job->state == IPP_JOB_ABORTED
		|| job->state == IPP_JOB_CANCELLED;
}

static void
job_changed_cb (ECCupsJobMonitor *mon, GnomeCupsJob *job, ECJobModel *model)
{
	GtkTreeIter iter;
	GnomeCupsJob *old_job = NULL;
	ECJobState state, new_state;
	gchar *printer;
	gint old_state = 0;
	guint32 local_jobid;

	rb_debug ("handling job %d changed", job->id);
	rb_debug ("state_reason: %s", job->state_reason);
	if (!ec_job_model_remote_jobid_to_iter (model, job->id, &iter)) {
		g_assert (ec_job_model_local_jobid_to_iter (model, job->id, &iter));
	}

	gtk_tree_model_get (GTK_TREE_MODEL (model), &iter,
			    EC_JOB_MODEL_COLUMN_JOB, &old_job,
			    EC_JOB_MODEL_COLUMN_LOCAL_JOBID, &local_jobid,
			    EC_JOB_MODEL_COLUMN_STATE, &state,
			    EC_JOB_MODEL_COLUMN_PRINTER, &printer,
			    -1);
	if (old_job) {
		old_state = old_job->state;
		gnome_cups_job_free (old_job);
	}

	if (job_is_done (job))
		new_state = EC_JOB_STATE_FINAL;
	else if (state_is_unknown (state))
		new_state = map_unknown_state_to_known (state);
	else 
		new_state = state;
	
	gtk_list_store_set (GTK_LIST_STORE (model), &iter,
			    EC_JOB_MODEL_COLUMN_STATE, new_state, 
			    EC_JOB_MODEL_COLUMN_JOB, gnome_cups_job_dup (job),
			    -1);
	
	if (old_state != job->state) 
		g_signal_emit (model, signals[STATE_CHANGED], 0,
			       printer, local_jobid, job);
}

static void
job_submitted_local_internal (ECJobModel *model,
			      const char *printer_name,
			      guint32 job_id,
			      GtkTreeIter *iter)
{
	g_return_if_fail (ec_job_model_local_jobid_to_iter (model, job_id, iter) == FALSE);

	rb_debug ("job %d queued local on %s\n", job_id, printer_name ? printer_name : "(UNKNOWN)");

	gtk_list_store_append (GTK_LIST_STORE (model), iter);
	gtk_list_store_set (GTK_LIST_STORE (model), iter,
			    EC_JOB_MODEL_COLUMN_STATE, EC_JOB_STATE_SUBMITTED,
			    EC_JOB_MODEL_COLUMN_PRINTER, g_strdup (printer_name),
			    EC_JOB_MODEL_COLUMN_LOCAL_JOBID, job_id,
			    -1);
}

void
ec_job_model_job_submitted_local (ECJobModel *model,
				  const char *printer_name,
				  guint32 job_id)
{
	GtkTreeIter iter;
	GTimeVal *current_time;

	job_submitted_local_internal (model, printer_name, job_id, &iter);

	current_time = g_new0 (GTimeVal, 1);
	g_get_current_time (current_time);
	
	gtk_list_store_set (GTK_LIST_STORE (model), &iter,
			    EC_JOB_MODEL_COLUMN_LOCAL_SUBMISSION_TIME, current_time,
			    EC_JOB_MODEL_COLUMN_LOCAL_SUBMISSION_TIME_RELATIVE, 0,
			    EC_JOB_MODEL_COLUMN_JOB, NULL,
			    -1);
}

void
ec_job_model_job_started_local (ECJobModel *model,
				const char *printer_name,
				guint32 job_id)
{
	GtkTreeIter iter;
	GTimeVal *current_time;
	GnomeCupsJob *job;
	char *printer_path;

	if (!ec_job_model_local_jobid_to_iter (model, job_id, &iter)) {
		rb_debug ("no local job for %d known, creating one");
		job_submitted_local_internal (model, printer_name, job_id, &iter);
	}

	rb_debug ("job %d destined for local printer %s\n", job_id,
		  printer_name ? printer_name : "(UNKNOWN)");

	current_time = g_new0 (GTimeVal, 1);
	g_get_current_time (current_time);

	gtk_list_store_set (GTK_LIST_STORE (model), &iter,
			    EC_JOB_MODEL_COLUMN_STATE, EC_JOB_STATE_LOCAL,
			    -1);

	/* Yes, this is broken... */
	printer_path = g_strdup_printf ("/printers/%s", printer_name);
	job = ec_cups_job_monitor_add_job (model->priv->mon, TRUE,
					   "localhost",
					   printer_path,
					   job_id);
	g_free (printer_path);
	if (job) {
		rb_debug ("job state: %d", job->state);
		rb_debug ("job state: %s", job->state_str);

		gtk_list_store_set (GTK_LIST_STORE (model), &iter,
				    EC_JOB_MODEL_COLUMN_JOB, job,
				    -1);
	}
	else {
		ec_cups_job_monitor_remove_job (model->priv->mon, "localhost", job_id);
		rb_debug ("no job for local job %d", job_id);
	}
}

void
ec_job_model_job_sent_remote (ECJobModel *model,
			      const char *printer_name,
			      guint32 local_job_id,
			      const char *printer_uri,
			      guint32 remote_job_id)
{
	xmlURIPtr xmluri;
	const char *host;
	const char *path;
	GtkTreeIter iter;
	GTimeVal *current_time;
	GnomeCupsJob *job;
	gboolean known_local_job;

	known_local_job = ec_job_model_local_jobid_to_iter (model, local_job_id, &iter);

	xmluri = xmlParseURI (printer_uri);
	if (!xmluri) {
	  g_critical ("invalid printer URI: %s", printer_uri);
	  return;
	}
	host = xmluri->server;
	path = xmluri->path;

	rb_debug ("local job %d%s sent remote on queue %s, remote id %d\n",
		  local_job_id,
		  known_local_job ? "" : " (unknown!)",
		  host, remote_job_id);

	job = ec_cups_job_monitor_add_job (model->priv->mon, TRUE,
					   host, path,
					   remote_job_id);
	if (job) {
		rb_debug ("job state: %d", job->state);
		rb_debug ("job state: %s", job->state_str);

		if (!known_local_job) {
			gtk_list_store_append (GTK_LIST_STORE (model), &iter);
			gtk_list_store_set (GTK_LIST_STORE (model), &iter,
					    EC_JOB_MODEL_COLUMN_PRINTER, g_strdup (printer_name),
					    EC_JOB_MODEL_COLUMN_LOCAL_SUBMISSION_TIME,
					    NULL,
					    EC_JOB_MODEL_COLUMN_LOCAL_SUBMISSION_TIME_RELATIVE,
					    0,
					    EC_JOB_MODEL_COLUMN_JOB, NULL,
					    -1);
		}

		current_time = g_new0 (GTimeVal, 1);
		g_get_current_time (current_time);
		gtk_list_store_set (GTK_LIST_STORE (model), &iter,
				    EC_JOB_MODEL_COLUMN_STATE,
				    job ? EC_JOB_STATE_REMOTE : EC_JOB_STATE_REMOTE_UNKNOWN,
				    EC_JOB_MODEL_COLUMN_REMOTE_SUBMISSION_TIME,
				    current_time,
				    EC_JOB_MODEL_COLUMN_REMOTE_SUBMISSION_TIME_RELATIVE,
				    0,
				    EC_JOB_MODEL_COLUMN_REMOTE_JOBID,
				    remote_job_id,
				    EC_JOB_MODEL_COLUMN_JOB, job,
				    -1);

		/* Now no need to monitor local job. */
		ec_cups_job_monitor_remove_job (model->priv->mon, "localhost", local_job_id);
	}
	else {
		ec_cups_job_monitor_remove_job (model->priv->mon, host, remote_job_id);
		rb_debug ("no job for local %d remote %d", local_job_id, remote_job_id);
	}
	xmlFreeURI (xmluri);
}

void
ec_job_model_remove_local_job (ECJobModel *model,
			       guint32 job_id)
{
	GtkTreeIter iter;
	GTimeVal *local_time;
	
	g_return_if_fail (ec_job_model_local_jobid_to_iter (model, job_id, &iter) == TRUE);

	gtk_tree_model_get (GTK_TREE_MODEL (model), &iter,
			    EC_JOB_MODEL_COLUMN_LOCAL_SUBMISSION_TIME,
			    &local_time, -1);
	g_free (local_time);
}

void
ec_job_model_remove_remote_job (ECJobModel *model,
				guint32 job_id)
{
	char *printer_name;
	GtkTreeIter iter;
	GnomeCupsJob *job;
	GTimeVal *local_time;
	GTimeVal *remote_time;
	
	g_return_if_fail (ec_job_model_remote_jobid_to_iter (model, job_id, &iter) == TRUE);

	gtk_tree_model_get (GTK_TREE_MODEL (model), &iter,
			    EC_JOB_MODEL_COLUMN_PRINTER,
			    &printer_name,
			    EC_JOB_MODEL_COLUMN_JOB,
			    &job,
			    EC_JOB_MODEL_COLUMN_LOCAL_SUBMISSION_TIME,
			    &local_time,
			    EC_JOB_MODEL_COLUMN_REMOTE_SUBMISSION_TIME,
			    &remote_time, -1);

	ec_cups_job_monitor_remove_job (model->priv->mon, printer_name, job_id);

	if (job)
		gnome_cups_job_free (job);
	g_free (local_time);
	g_free (remote_time);
	g_free (printer_name);
}

gint
ec_job_model_job_get_pending_state (ECJobModel *model, 
				    guint32     local_job_id)
{
	GtkTreeIter iter;
	gint pending;
	
	ec_job_model_local_jobid_to_iter (model, local_job_id, &iter);

	gtk_tree_model_get (GTK_TREE_MODEL (model), &iter,
			    EC_JOB_MODEL_COLUMN_PENDING_STATE, &pending,
			    -1);

	return pending;
}

void
ec_job_model_job_set_pending_state (ECJobModel *model, 
				    guint32     local_job_id,
				    gint        pending_state)
{
	GtkTreeIter iter;
	
	ec_job_model_local_jobid_to_iter (model, local_job_id, &iter);

	gtk_list_store_set (GTK_LIST_STORE (model), &iter,
			    EC_JOB_MODEL_COLUMN_PENDING_STATE, pending_state,
			    -1);
}

void
ec_job_model_remove_old_completed (ECJobModel *model, long old_delta)
{
	GtkTreeModel *treemodel;
	GtkTreeIter iter;
	long delta;
	GnomeCupsJob *job;

	treemodel = GTK_TREE_MODEL (model);
	if (!gtk_tree_model_get_iter_first (treemodel, &iter))
		return;
	
	do {	
		gtk_tree_model_get (treemodel, &iter,
				    EC_JOB_MODEL_COLUMN_JOB, &job,
				    EC_JOB_MODEL_COLUMN_LOCAL_SUBMISSION_TIME_RELATIVE,
				    &delta, -1);
		if (job && !job_is_done (job))
			continue;
		if (delta > old_delta) {
			if (gtk_list_store_remove (GTK_LIST_STORE (model), &iter))
				continue;
			else
				break;
		}
	} while (gtk_tree_model_iter_next (treemodel, &iter));
}

/* This should really be standard. */
#define ENUM_ENTRY(NAME, DESC) { NAME, "" #NAME "", DESC }

GType
ec_job_state_get_type (void)
{
	static GType etype = 0;

	if (etype == 0)
	{
		static const GEnumValue values[] =
		{

			ENUM_ENTRY (EC_JOB_STATE_SUBMITTED, "Submitted"),
			ENUM_ENTRY (EC_JOB_STATE_SUBMITTED_UNKNOWN, "Submitted (timeout)"),
			ENUM_ENTRY (EC_JOB_STATE_LOCAL, "Local"),
			ENUM_ENTRY (EC_JOB_STATE_LOCAL_UNKNOWN, "Local (timeout)"),
			ENUM_ENTRY (EC_JOB_STATE_REMOTE, "Remote"),
			ENUM_ENTRY (EC_JOB_STATE_REMOTE_UNKNOWN, "Remote (timeout)"),
			ENUM_ENTRY (EC_JOB_STATE_FINAL, "That's all folks!"),
			ENUM_ENTRY (EC_JOB_STATE_FINAL_TIMEOUT, "That's all folks! (timeout)"),
			{ 0, 0, 0 }
		};

		etype = g_enum_register_static ("ECJobState", values);
	}

	return etype;
}

