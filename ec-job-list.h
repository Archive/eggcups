/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2004 Red Hat, Inc.
 * Written by Matthias Clasen <mclasen@redhat.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __EC_JOB_LIST_H__
#define __EC_JOB_LIST_H__

#include "config.h"
#include "ec-job-model.h"
#include <libgnomeui/gnome-app.h>

G_BEGIN_DECLS

#define EC_TYPE_JOB_LIST         (ec_job_list_get_type ())
#define EC_JOB_LIST(o)           (G_TYPE_CHECK_INSTANCE_CAST ((o), EC_TYPE_JOB_LIST, ECJobList))
#define EC_JOB_LIST_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST((k), EC_TYPE_JOB_LIST, ECJobListClass))
#define EC_IS_JOB_LIST(o)        (G_TYPE_CHECK_INSTANCE_TYPE ((o), EC_TYPE_JOB_LIST))
#define EC_IS_JOB_LIST_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), EC_TYPE_JOB_LIST))
#define EC_JOB_LIST_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), EC_TYPE_JOB_LIST, ECJobListClass)) 

typedef struct ECJobListPrivate ECJobListPrivate;

typedef struct
{
	GnomeApp parent;

	ECJobListPrivate *priv;
} ECJobList;

typedef struct
{
	GnomeAppClass parent;
} ECJobListClass;

GType ec_job_list_get_type (void);

ECJobList *ec_job_list_new (ECJobModel *job_model);

G_END_DECLS

#endif /* __EC_JOB_LIST_H__ */
