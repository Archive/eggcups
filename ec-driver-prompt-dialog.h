/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2004 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __EC_DRIVER_PROMPT_DIALOG__
#define __EC_DRIVER_PROMPT_DIALOG__

#include <gtk/gtkdialog.h>
#include <dbus/dbus.h>
#include "config.h"
#include <libxml/parser.h>

G_BEGIN_DECLS

#define EC_DRIVER_PROMPT_PATH      "/com/redhat/PrintDriverSelection"
#define EC_DRIVER_PROMPT_NAMESPACE "com.redhat.PrintDriverSelection"
#define EC_DRIVER_FOOMATIC_PATH    "/usr/bin/"

#define CUPS_DRIVER_CONFIG_PATH      "/com/redhat/CupsDriverConfig"
#define CUPS_DRIVER_CONFIG_NAMESPACE "com.redhat.CupsDriverConfig"

#define HAL_NAMESPACE         "org.freedesktop.Hal"
#define HAL_PRINTER_INTERFACE "org.freedesktop.Hal.Device.Printer"
#define HAL_PRINTER_CONFIG_METHOD "Configure"

#define EC_GCONF_PRINTER_PATH      "/system/printing/user_drivers"

#define EC_GCONF_ALLOWED_CHARS     "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_"

typedef struct ECDriverPromptDialogPrivate ECDriverPromptDialogPrivate;

typedef struct _ECDriverPromptDialog 
{
	GtkDialog parent;

	ECDriverPromptDialogPrivate *priv;
} ECDriverPromptDialog;

typedef struct _ECDriverPromptDialogClass
{
	GtkDialogClass parent;
} ECDriverPromptDialogClass;

#define EC_TYPE_DRIVER_PROMPT_DIALOG (ec_driver_prompt_dialog_get_type())
#define EC_DRIVER_PROMPT_DIALOG(object) (G_TYPE_CHECK_INSTANCE_CAST((object), EC_TYPE_DRIVER_PROMPT_DIALOG, ECDriverPromptDialog))
#define EC_DRIVER_PROMPT_DIALOG_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass), EC_TYPE_DRIVER_PROMPT_DIALOG, ECDriverPromptDialogClass))
#define EC_IS_DRIVER_PROMPT_DIALOG(object) (G_TYPE_CHECK_INSTANCE_TYPE((object), EC_TYPE_DRIVER_PROMPT_DIALOG))
#define EC_IS_DRIVER_PROMPT_DIALOG_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), EC_TYPE_DRIVER_PROMPT_DIALOG))
#define EC_DRIVER_PROMPT_DIALOG_GET_CLASS(object) (G_TYPE_INSTANCE_GET_CLASS((object), EC_TYPE_DRIVER_PROMPT_DIALOG, ECDriverPromptDialogClass))

GType ec_driver_prompt_dialog_get_type			(void);

ECDriverPromptDialog *ec_driver_prompt_dialog_new 	(void);

gboolean ec_driver_prompt_dialog_parse_message		(ECDriverPromptDialog *dlg,
							 DBusMessage *msg);

void ec_driver_prompt_get_user_driver 			(DBusConnection *connection, 
                                                 	 DBusMessage *message);

gboolean dbus_set_printer_driver 			(const gchar *make,
                         			 	 const gchar *model,
                         				 const gchar *udi,
                         				 const gchar *name,
                         				 DBusError *error);

void ec_driver_prompt_dialog_set_and_show 		(ECDriverPromptDialog *dlg,
				      			const gchar *make,
				      			const gchar *model,
				      			const gchar *printer_udi,
				      			const gchar *printer_name);



G_END_DECLS

#endif
