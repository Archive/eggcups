/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2004 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "ec-driver-prompt-dialog.h"
#include <stdio.h>
#include <glade/glade.h>
#include <gtk/gtk.h>
#include <string.h>
#include <gconf/gconf-client.h>
#include "rb-debug.h"

G_DEFINE_TYPE(ECDriverPromptDialog, ec_driver_prompt_dialog, GTK_TYPE_DIALOG)

static GObject *ec_driver_prompt_dialog_constructor (GType type,
						     guint n_construct_properties,
						     GObjectConstructParam *construct_properties);
static void ec_driver_prompt_dialog_finalize (GObject *obj);
static void ec_driver_prompt_dialog_set_property (GObject *object,
						  guint prop_id,
						  const GValue *value,
						  GParamSpec *pspec);
static void ec_driver_prompt_dialog_get_property (GObject *object,
						  guint prop_id,
						  GValue *value,
						  GParamSpec *pspec);
static void on_ok_button_clicked (GtkButton *button, gpointer user_data);
static void on_cancel_button_clicked (GtkButton *button, gpointer user_data);
static gint default_tree_sort_on_first_col_func (GtkTreeModel *model,
						 GtkTreeIter *a,
						 GtkTreeIter *b,
						 gpointer user_data);
static void on_make_selection_changed (GtkTreeSelection *selection, 
				       gpointer user_data);
static void on_model_selection_changed (GtkTreeSelection *selection, 
					gpointer user_data);

enum {
	PROP_NONE,
};

typedef enum _ECDriverParseState {
	PARSE_STATE_START,
	PARSE_STATE_PRINTER,
	PARSE_STATE_MAKE,
	PARSE_STATE_MODEL,
	PARSE_STATE_DRIVER,
	PARSE_STATE_DONE
} ECDriverParseState;

typedef struct _ECDriverData {
	gchar *make;
	gchar *model;
	gchar *driver;
	gchar *last_matched_model;
} ECDriverData;

struct ECDriverPromptDialogPrivate {
	GtkWidget *make_treeview;
	GtkWidget *model_treeview;
	GtkWidget *ok_button;
	GtkWidget *cancel_button;
	GtkWidget *info_label;

	DBusMessage *message;

	gchar *printer_udi;
	gchar *printer_name;
	gchar *printer_make;
	gchar *printer_model;

	xmlParserCtxtPtr foomatic_xml_ctx;
	ECDriverParseState parse_state;
	ECDriverData current_driver;
	GHashTable *make_hash;
	gboolean user_selected;

	gint io_id;
};

static void 
ec_driver_prompt_dialog_init (ECDriverPromptDialog *dp)
{
	dp->priv = g_new0 (struct ECDriverPromptDialogPrivate, 1);
}

static GObjectClass *parent_class = NULL;

static void 
ec_driver_prompt_dialog_class_init (ECDriverPromptDialogClass *klass)
{
	GObjectClass *gobject_class;
	gobject_class = G_OBJECT_CLASS (klass);

	parent_class = G_OBJECT_CLASS (g_type_class_peek_parent (gobject_class));

	gobject_class->constructor = ec_driver_prompt_dialog_constructor;
	gobject_class->finalize = ec_driver_prompt_dialog_finalize;

	gobject_class->set_property = ec_driver_prompt_dialog_set_property;
	gobject_class->get_property = ec_driver_prompt_dialog_get_property;

}

static GObject *
ec_driver_prompt_dialog_constructor (GType type,
				     guint n_props,
				     GObjectConstructParam *construct_props)
{
	GObject *obj;
	ECDriverPromptDialog *dlg;
	ECDriverPromptDialogClass *klass;
	char *glade_path;
	GtkListStore *make_store;
	GtkCellRenderer *renderer;
	GtkTreeViewColumn *column;
	GtkTreeSelection *select;
	GtkWidget *main_vbox;
	GtkWidget *dummy_glade_dialog;
	GladeXML *xml;

	klass = EC_DRIVER_PROMPT_DIALOG_CLASS (g_type_class_peek (type));
	obj = parent_class->constructor (type,
                                         n_props,
                                         construct_props);
	dlg = EC_DRIVER_PROMPT_DIALOG (obj);

	gtk_window_set_default_size (GTK_WINDOW (dlg), 400, 400);

	glade_path = g_build_filename (GLADEDIR, "driverdialog.glade", NULL);

	xml = glade_xml_new (glade_path, NULL, NULL);
	g_free (glade_path);

	main_vbox  = glade_xml_get_widget (xml, "main_vbox");
	dummy_glade_dialog = glade_xml_get_widget (xml, "driver_dialog");

	gtk_widget_reparent (main_vbox, GTK_DIALOG(dlg)->vbox);

	dlg->priv->make_treeview = glade_xml_get_widget (xml, "make_treeview");
	dlg->priv->model_treeview = glade_xml_get_widget (xml, "model_treeview");

	dlg->priv->cancel_button = gtk_button_new_from_stock (GTK_STOCK_CANCEL);
	dlg->priv->ok_button = gtk_button_new_from_stock (GTK_STOCK_OK);

	gtk_dialog_add_action_widget (GTK_DIALOG (dlg),
				      dlg->priv->cancel_button,
				      GTK_RESPONSE_REJECT);
	
	gtk_dialog_add_action_widget (GTK_DIALOG (dlg),
				      dlg->priv->ok_button,
				      GTK_RESPONSE_ACCEPT);

	dlg->priv->info_label = glade_xml_get_widget (xml, "info_label");
	g_signal_connect (dlg->priv->ok_button, "clicked", 
                          (GCallback) on_ok_button_clicked, dlg);
	g_signal_connect (dlg->priv->cancel_button, "clicked", 
                          (GCallback) on_cancel_button_clicked, dlg);
	make_store = gtk_list_store_new (1, G_TYPE_STRING);
	gtk_tree_view_set_model (GTK_TREE_VIEW (dlg->priv->make_treeview), 
                                 GTK_TREE_MODEL(make_store));

	gtk_tree_sortable_set_default_sort_func (GTK_TREE_SORTABLE (make_store),
                                                 (GtkTreeIterCompareFunc) default_tree_sort_on_first_col_func,
                                                 NULL,
                                                 NULL);

	gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE (make_store),
                                              GTK_TREE_SORTABLE_DEFAULT_SORT_COLUMN_ID,
                                              GTK_SORT_ASCENDING);

	select = gtk_tree_view_get_selection (GTK_TREE_VIEW (dlg->priv->make_treeview));
	gtk_tree_selection_set_mode (select, GTK_SELECTION_SINGLE);
	
	g_signal_connect (G_OBJECT (select), "changed",
                          (GCallback) on_make_selection_changed, dlg);


	select = gtk_tree_view_get_selection (GTK_TREE_VIEW (dlg->priv->model_treeview));
	gtk_tree_selection_set_mode (select, GTK_SELECTION_SINGLE);
	
	g_signal_connect (G_OBJECT (select), "changed",
                          (GCallback) on_model_selection_changed, dlg);


	renderer = gtk_cell_renderer_text_new ();

	column = gtk_tree_view_column_new_with_attributes ("Make",
                                                   renderer,
                                                   "text", 0,
                                                   NULL);

	gtk_tree_view_append_column (GTK_TREE_VIEW (dlg->priv->make_treeview), 
                                     column);

	column = gtk_tree_view_column_new_with_attributes ("Model",
                                                   renderer,
                                                   "text", 0,
                                                   NULL);

	gtk_tree_view_append_column (GTK_TREE_VIEW (dlg->priv->model_treeview), 
                                     column);

	gtk_widget_destroy (dummy_glade_dialog);
	g_object_unref (G_OBJECT (make_store));
	g_object_unref (xml);

	return obj;	
}

static void 
ec_driver_prompt_dialog_set_property (GObject *object,
                            guint prop_id,
                            const GValue *value,
                            GParamSpec *pspec)
{
	ECDriverPromptDialog *dlg;

	dlg = EC_DRIVER_PROMPT_DIALOG (object);

	switch (prop_id)
	{
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
			break;
	}
}

static void
ec_driver_prompt_dialog_get_property (GObject *object,
				      guint prop_id,
				      GValue *value,
				      GParamSpec *pspec)
{
	ECDriverPromptDialog *dlg;

	dlg = EC_DRIVER_PROMPT_DIALOG (object);

	switch (prop_id)
	{
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
			break;
	}

}

static void
ec_driver_prompt_dialog_finalize (GObject *object)
{
	ECDriverPromptDialog *dlg = EC_DRIVER_PROMPT_DIALOG (object);

	g_return_if_fail (dlg->priv != NULL);

	rb_debug ("finalizing");

	if (dlg->priv->io_id != 0)
		g_source_remove (dlg->priv->io_id);

	dlg->priv->parse_state = PARSE_STATE_DONE;

	if (dlg->priv->foomatic_xml_ctx != NULL) {
		xmlFreeParserCtxt(dlg->priv->foomatic_xml_ctx);
	}

	xmlCleanupParser();

	g_free (dlg->priv->printer_udi);
	g_free (dlg->priv->printer_name);
	g_free (dlg->priv->printer_make);
	g_free (dlg->priv->printer_model);

	g_free (dlg->priv->current_driver.make);
	g_free (dlg->priv->current_driver.model);
	g_free (dlg->priv->current_driver.driver);	
	g_free (dlg->priv->current_driver.last_matched_model);

	g_hash_table_destroy (dlg->priv->make_hash);

	g_free (dlg->priv);

	parent_class->finalize (object);
	rb_debug ("done");
}

ECDriverPromptDialog *
ec_driver_prompt_dialog_new (void)
{
	return g_object_new (EC_TYPE_DRIVER_PROMPT_DIALOG, NULL);
}

static gint
default_tree_sort_on_first_col_func (GtkTreeModel *model,
                                     GtkTreeIter *a,
                                     GtkTreeIter *b,
                                     gpointer user_data)
{
	gchar *a_val, *b_val;
	gint result;

	gtk_tree_model_get (model, a, 0, &a_val, -1);
	gtk_tree_model_get (model, b, 0, &b_val, -1);

	result = g_ascii_strcasecmp (a_val, b_val);

	g_free (a_val);
	g_free (b_val);

	return result;
}

static void
string_key_destroy (gpointer key)
{
	gchar *ckey = key;

	g_free (ckey);
}

static void
store_value_unref (gpointer value)
{
	GObject *store = G_OBJECT (value);

	g_object_unref (store);
}


static void
processStartDoc (void *user_data)
{
	ECDriverPromptDialog *dlg;

	rb_debug ("Start Document");
	dlg = EC_DRIVER_PROMPT_DIALOG (user_data);
	dlg->priv->parse_state = PARSE_STATE_START;
	dlg->priv->make_hash = g_hash_table_new_full (g_str_hash, 
						      g_str_equal, 
						      (GDestroyNotify) string_key_destroy, 
						      (GDestroyNotify) store_value_unref);
	dlg->priv->user_selected = FALSE;
	dlg->priv->current_driver.last_matched_model = NULL;

	dlg->priv->current_driver.make = NULL;
	dlg->priv->current_driver.model = NULL;
	dlg->priv->current_driver.driver = NULL;
}

static void
processEndDoc (void *user_data)
{
	GtkTreeSelection *selection;
	GtkTreeIter iter;
	GtkTreeModel *tree_model;
	ECDriverPromptDialog *dlg;

	dlg = EC_DRIVER_PROMPT_DIALOG (user_data);

	rb_debug ("End Document");

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (dlg->priv->make_treeview));

	if (gtk_tree_selection_get_selected (selection, &tree_model, &iter)) {
		GtkTreePath *path;

		path = gtk_tree_model_get_path  (tree_model, &iter);

		gtk_tree_view_scroll_to_cell (GTK_TREE_VIEW (dlg->priv->make_treeview),
                                              path,
                                              NULL,
                                              FALSE,
                                              0,
                                              0);
		gtk_tree_path_free (path);
	}

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (dlg->priv->model_treeview));

	if (gtk_tree_selection_get_selected (selection, &tree_model, &iter)) {
		GtkTreePath *path;

		path = gtk_tree_model_get_path  (tree_model, &iter);

		gtk_tree_view_scroll_to_cell (GTK_TREE_VIEW (dlg->priv->model_treeview),
                                              path,
                                              NULL,
                                              FALSE,
                                              0,
                                              0);
		gtk_tree_path_free (path);
	}

}

static void
processStart (void *user_data, 
              const xmlChar *name, 
              const xmlChar **atts)
{
	ECDriverPromptDialog *dlg;

	dlg = EC_DRIVER_PROMPT_DIALOG (user_data);

	rb_debug ("Start: %s", name);

	if (strcmp ((gchar*) name, "printer") == 0)
		dlg->priv->parse_state = PARSE_STATE_PRINTER;
	else if (strcmp ((gchar*) name, "make") == 0)
		dlg->priv->parse_state = PARSE_STATE_MAKE;
	else if (strcmp ((gchar*) name, "model") == 0)
		dlg->priv->parse_state = PARSE_STATE_MODEL;
	else if (strcmp ((gchar*) name, "driver") == 0)
		dlg->priv->parse_state = PARSE_STATE_DRIVER;

}

/* 
if the user has not yet selected a Manufacturer(make) 
and the make we are populating the list with equals 
the make given to us by the printer, select that line
in the list. 
*/
static void
driver_prompt_dialog_higlight_make_match (ECDriverPromptDialog *dlg, 
                                          GtkTreeIter *iter) 
{
	if (!dlg->priv->user_selected) {
		if (g_ascii_strcasecmp (dlg->priv->printer_make,  
                                        dlg->priv->current_driver.make) == 0) {
			GtkTreeSelection *select;

			select = gtk_tree_view_get_selection (GTK_TREE_VIEW (dlg->priv->make_treeview));
			gtk_tree_selection_select_iter (select, iter);
		}
	}
}

static void
driver_prompt_dialog_higlight_model_match (ECDriverPromptDialog *dlg, 
                                           GtkTreeIter *iter)
{
	if (g_ascii_strcasecmp (dlg->priv->printer_make,  
            dlg->priv->current_driver.make) == 0) {
		GtkTreeSelection *select;
		gboolean select_current;
		select_current = FALSE;

		/*best guess for model*/
		if (dlg->priv->current_driver.last_matched_model == NULL) {
			dlg->priv->current_driver.last_matched_model = g_strdup (dlg->priv->current_driver.model);
		} else {
			gchar *a = dlg->priv->printer_model;
			gchar *b = dlg->priv->current_driver.model;
			gchar *c = dlg->priv->current_driver.last_matched_model;
			gint ba_result,ca_result, bc_result;

			ba_result = g_ascii_strcasecmp (b, a);
			ca_result = g_ascii_strcasecmp (c, a);
			bc_result = g_ascii_strcasecmp (b, c); 

			if (ca_result == 0 ) {
				/*NOOP: we already found a match so no need to check further*/
			} else if (ba_result == 0) {
				g_free (dlg->priv->current_driver.last_matched_model);
				dlg->priv->current_driver.last_matched_model = g_strdup (a);
				select_current = TRUE;
			} else if (ba_result < 0 && ca_result < 0) {
				if (bc_result > 0) {
					g_free (dlg->priv->current_driver.last_matched_model);
					dlg->priv->current_driver.last_matched_model = g_strdup (b);
					select_current = TRUE;
				}
			} else if (ba_result > 0 && ca_result > 0) {
				if (bc_result < 0) {
					g_free (dlg->priv->current_driver.last_matched_model);
					dlg->priv->current_driver.last_matched_model = g_strdup (b);
					select_current = TRUE;
				}
			} else if (bc_result < 0) {
				g_free (dlg->priv->current_driver.last_matched_model);
				dlg->priv->current_driver.last_matched_model = g_strdup (b);
				select_current = TRUE;
			}
		}

		if (select_current) {
			select = gtk_tree_view_get_selection (GTK_TREE_VIEW (dlg->priv->model_treeview));
			gtk_tree_selection_select_iter (select, iter);
		}
	}
}


static void
processEnd (void *user_data, 
            const xmlChar *name)
{
	ECDriverPromptDialog *dlg;

	rb_debug ("End: %s", name);
	dlg = EC_DRIVER_PROMPT_DIALOG (user_data);

	if (strcmp ((gchar*) name, "printer") == 0) {
		GtkListStore *model_store;
		GtkTreeIter iter;

		dlg->priv->parse_state = PARSE_STATE_START;

		model_store = GTK_LIST_STORE (g_hash_table_lookup (dlg->priv->make_hash, dlg->priv->current_driver.make));
		if (model_store == NULL) {
			GtkListStore *make_store;

			model_store = gtk_list_store_new (2, G_TYPE_STRING, G_TYPE_STRING);

			gtk_tree_sortable_set_default_sort_func 
                                                 (GTK_TREE_SORTABLE (model_store),
                                                 (GtkTreeIterCompareFunc) default_tree_sort_on_first_col_func,
                                                 NULL, NULL);

			gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE (model_store),
                                                              GTK_TREE_SORTABLE_DEFAULT_SORT_COLUMN_ID,
                                                              GTK_SORT_ASCENDING);


			g_hash_table_insert (dlg->priv->make_hash, g_strdup(dlg->priv->current_driver.make), model_store);

			make_store = GTK_LIST_STORE (gtk_tree_view_get_model (GTK_TREE_VIEW(dlg->priv->make_treeview)));
			gtk_list_store_append (make_store, &iter);
			gtk_list_store_set (make_store, &iter, 0, dlg->priv->current_driver.make, -1);

			driver_prompt_dialog_higlight_make_match (dlg, &iter);
		} 

		gtk_list_store_append (model_store, &iter);
		gtk_list_store_set (model_store, &iter, 
                                    0, dlg->priv->current_driver.model, 
                                    1, dlg->priv->current_driver.driver,
                                    -1);
		
		driver_prompt_dialog_higlight_model_match (dlg, &iter);

		g_free (dlg->priv->current_driver.make);
		g_free (dlg->priv->current_driver.model);
		g_free (dlg->priv->current_driver.driver);

		dlg->priv->current_driver.make = NULL;
		dlg->priv->current_driver.model = NULL;
		dlg->priv->current_driver.driver = NULL;
	} 
	else if (strcmp ((gchar*) name, "make") == 0 ||
                 strcmp ((gchar*) name, "model") == 0 ||
                 strcmp ((gchar*) name, "driver") == 0) {
		dlg->priv->parse_state = PARSE_STATE_PRINTER;
	}


}

static void
processCData (void *user_data,
              const xmlChar *cdata,
              int len)
{
	gchar *text;

	text = g_strndup ((gchar*) cdata, len);
	rb_debug ("CData: %s", text);

	g_free (text);
}

static void
processChars (void *user_data,
              const xmlChar *chars,
              int len)
{
	ECDriverPromptDialog *dlg;
	gchar *text;

	text = g_strndup ((gchar*) chars, len);
	rb_debug ("Chars: %s\n", text);

	dlg = EC_DRIVER_PROMPT_DIALOG (user_data);

	switch (dlg->priv->parse_state) {
	case PARSE_STATE_MAKE:
		if (!dlg->priv->current_driver.make)
			dlg->priv->current_driver.make = text;
		break;
	case PARSE_STATE_MODEL:
		if (!dlg->priv->current_driver.model)
			dlg->priv->current_driver.model = text;
		break;
	case PARSE_STATE_DRIVER:
		g_free (dlg->priv->current_driver.driver);
		dlg->priv->current_driver.driver = text;
		break;
	case PARSE_STATE_START:
		break;
	case PARSE_STATE_PRINTER:
		break;
	case PARSE_STATE_DONE:
	 	break;	
	}
}

xmlSAXHandler saxh = {
	NULL, /* internalSubset */
	NULL, /* isStandalone */
	NULL, /* hasInternalSubset */
	NULL, /* hasExternalSubset */
	NULL, /* resolveEntity */
	NULL, /* getEntity */
	NULL, /* entityDecl */
	NULL, /* notationDecl */
	NULL, /* attributeDecl */
	NULL, /* elementDecl */
	NULL, /* unparsedEntityDecl */
	NULL, /* setDocumentLocator */
	processStartDoc, /* startDocument */
	processEndDoc, /* endDocument */
	processStart, /* startElement */
	processEnd,   /* endElement */
	NULL, /* reference */
	processChars, /* characters */
	NULL, /* ignorableWhitespace */
	NULL, /* processingInstruction */
	NULL, /* comment */
	NULL, /* xmlParserWarning */
	NULL, /* xmlParserError */
	NULL, /* xmlParserError */
	NULL, /* getParameterEntity */
	processCData, /* cdataBlock; */
	NULL, /* externalSubset; */
	1,
	NULL,
	NULL, /* startElementNs */
	NULL, /* endElementNs */
	NULL  /* xmlStructuredErrorFunc */
};

static gboolean
process_foomatic_data (GIOChannel *source,
                       GIOCondition condition,
                       gpointer user_data)
{
	gchar chars[128];
	gboolean keep_processing;
	GError *error;
	ECDriverPromptDialog *dlg;

	dlg = EC_DRIVER_PROMPT_DIALOG (user_data);

	if (dlg->priv->parse_state == PARSE_STATE_DONE) {
		g_io_channel_shutdown (source, FALSE, NULL);
		g_io_channel_unref (source);
		return FALSE;
	}

	keep_processing = TRUE;

	if (condition & G_IO_IN || condition & G_IO_PRI) {
		gsize length;
		GIOStatus status;

		error = NULL;
		status = g_io_channel_read_chars (source, chars, 128, &length, &error);

		switch (status) {
		case G_IO_STATUS_ERROR:
			g_error ("Error processing foomatic data: %s\n", error->message);
			g_error_free (error);
			keep_processing = FALSE;
			break;
		case G_IO_STATUS_EOF:
			rb_debug ("EOF of foomatic data encountered");
			keep_processing = FALSE;
			break;
		case G_IO_STATUS_NORMAL:
			if (dlg->priv->foomatic_xml_ctx == NULL)
				dlg->priv->foomatic_xml_ctx = xmlCreatePushParserCtxt (&saxh, dlg, chars, length, NULL);
			else
				xmlParseChunk (dlg->priv->foomatic_xml_ctx, chars, length, 0);
			break;
		case G_IO_STATUS_AGAIN:
			break;
		}
	} else {
		rb_debug ("Condition recived %d", condition);
		rb_debug ("Condition G_IO_IN = %d, G_IO_PRI = %d, G_IO_ERR = %d, G_IO_HUP = %d, G_IO_NVAL = %d", 
                           G_IO_IN, G_IO_PRI, G_IO_ERR, G_IO_HUP, G_IO_NVAL);

		keep_processing = FALSE;
	}

	if (!keep_processing) {
		rb_debug ("End XML processing");
		xmlParseChunk (dlg->priv->foomatic_xml_ctx, chars, 0, 1);
		xmlFreeParserCtxt(dlg->priv->foomatic_xml_ctx);
		dlg->priv->foomatic_xml_ctx = NULL;
	
		g_io_channel_shutdown (source, FALSE, NULL);
		g_io_channel_unref (source);
		return FALSE;
	}

	return TRUE;
}

static void
populate_dialog (ECDriverPromptDialog *dlg)
{
	GError *error;
	gint sout;

	char *argv[] = {EC_DRIVER_FOOMATIC_PATH"foomatic-configure", "-O", NULL};

	if (g_spawn_async_with_pipes (EC_DRIVER_FOOMATIC_PATH,
                                      argv,
                                      NULL,
                                      G_SPAWN_STDERR_TO_DEV_NULL,
                                      NULL, NULL, NULL, NULL,
                                      &sout,
                                      NULL,
                                      &error)) {
		GIOChannel *foomatic_data;
		foomatic_data = g_io_channel_unix_new (sout);

		dlg->priv->foomatic_xml_ctx = NULL;

		dlg->priv->io_id = g_io_add_watch_full (foomatic_data,
                                                        GDK_PRIORITY_REDRAW,
                                                        G_IO_IN | G_IO_PRI | G_IO_ERR | G_IO_NVAL | G_IO_HUP, 
                                                        (GIOFunc) process_foomatic_data, 
                                                        dlg,
                                                        NULL);
	}
}

static void
on_model_selection_changed (GtkTreeSelection *selection, 
                           gpointer user_data)
{
	GtkTreeIter iter;
	GtkTreeModel *tree_model;
	ECDriverPromptDialog *dlg;

	dlg = EC_DRIVER_PROMPT_DIALOG (user_data);

	rb_debug ("Model selection changed");

	if (gtk_tree_selection_get_selected (selection, &tree_model, &iter)) {

		gtk_widget_set_sensitive (dlg->priv->ok_button, TRUE);
        } else {
		gtk_widget_set_sensitive (dlg->priv->ok_button, FALSE);
	}
}


static void
on_make_selection_changed (GtkTreeSelection *selection, 
                           gpointer user_data)
{
	GtkTreeIter iter;
	GtkTreeModel *tree_model;
	gchar *make;
	ECDriverPromptDialog *dlg;
	GtkListStore *model_store;

	dlg = EC_DRIVER_PROMPT_DIALOG (user_data);

	rb_debug ("Make selection changed");

	gtk_widget_set_sensitive (dlg->priv->ok_button, FALSE);

	if (gtk_tree_selection_get_selected (selection, &tree_model, &iter)) {
		gtk_tree_model_get (tree_model, &iter, 0, &make, -1);

		model_store = GTK_LIST_STORE (g_hash_table_lookup (dlg->priv->make_hash, make));

		if (model_store != NULL) {
			gtk_tree_view_set_model (GTK_TREE_VIEW (dlg->priv->model_treeview),
                                                 GTK_TREE_MODEL (model_store));
		}

		if (dlg->priv->user_selected == FALSE) {
			GtkTreePath *path;
			path = gtk_tree_model_get_path  (tree_model, &iter);

			gtk_tree_view_scroll_to_cell (GTK_TREE_VIEW (dlg->priv->make_treeview),
                        	                      path,
                                	              NULL,
                                                      FALSE,
                                                      0,
                                                      0);

			gtk_tree_path_free (path);
		}

		g_free (make);
        }

	dlg->priv->user_selected = TRUE;

}

static gchar *
gconf_generate_user_driver_path (const gchar *make, const gchar *model)
{
	gchar *path;
	gchar *lc_make, *lc_model;
	
	lc_make = g_ascii_strdown (make, -1);
	lc_model = g_ascii_strdown (model, -1);

	g_strcanon (lc_make, EC_GCONF_ALLOWED_CHARS, '_');
	g_strcanon (lc_model, EC_GCONF_ALLOWED_CHARS, '_');

	path = g_strconcat (EC_GCONF_PRINTER_PATH, "/", lc_make, "_", lc_model, NULL);

	g_free (lc_make);
	g_free (lc_model);

	return path;
}

static void
gconf_set_printer_driver (gchar *reported_make,
                          gchar *reported_model,
			  gchar *selected_make,
			  gchar *selected_model,
                          gchar *selected_driver)
{
	gchar *path, *make_key, *model_key, *driver_key;
	GConfClient *gconf_client;

	path = gconf_generate_user_driver_path (reported_make, reported_model);
	make_key = g_strconcat (path, "/make", NULL);
	model_key = g_strconcat (path, "/model", NULL);
	driver_key = g_strconcat (path, "/driver", NULL);

	gconf_client = gconf_client_get_default ();
	gconf_client_set_string (gconf_client, make_key, selected_make, NULL);
	gconf_client_set_string (gconf_client, model_key, selected_model, NULL);
	gconf_client_set_string (gconf_client, driver_key, selected_driver, NULL);

	g_free (path);
	g_free (make_key);
	g_free (model_key);
	g_free (driver_key);
	g_object_unref (gconf_client);
}

gboolean
dbus_set_printer_driver (const gchar *make,
                         const gchar *model,
                         const gchar *udi,
                         const gchar *name,
                         DBusError   *error)
{
	DBusConnection *connection;
	DBusMessage *message;
	DBusMessageIter iter;

	connection = dbus_bus_get (DBUS_BUS_SYSTEM, error);
	if (connection == NULL) 
		return FALSE;

	message = dbus_message_new_method_call (HAL_NAMESPACE,
        	                                udi,
                                                HAL_PRINTER_INTERFACE,
                	                        HAL_PRINTER_CONFIG_METHOD);
				
	dbus_message_iter_init_append (message, &iter);
	dbus_message_iter_append_basic (&iter, DBUS_TYPE_STRING, &make);
	dbus_message_iter_append_basic (&iter, DBUS_TYPE_STRING, &model);

	if (!dbus_connection_send (connection, message, NULL))
		return FALSE;

  	dbus_connection_flush (connection);
  	dbus_message_unref (message);
  	dbus_connection_unref (connection);

	return TRUE;
}

static void
on_ok_button_clicked (GtkButton *button,
                      gpointer user_data)
{
	ECDriverPromptDialog *dlg;
	DBusError error;
	GtkTreeIter iter;
	GtkTreeModel *tree_model;
	GtkTreeSelection *selection;
	gchar *selected_driver = NULL;
	gchar *selected_make = NULL;
	gchar *selected_model = NULL;

	dlg = EC_DRIVER_PROMPT_DIALOG (user_data);

	rb_debug ("ok button clicked");

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (dlg->priv->make_treeview));
	if (gtk_tree_selection_get_selected (selection, &tree_model, &iter)) 
		gtk_tree_model_get (tree_model, &iter, 0, &selected_make, -1);
	
	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (dlg->priv->model_treeview));

	if (gtk_tree_selection_get_selected (selection, &tree_model, &iter)) {
		gtk_tree_model_get (tree_model, &iter, 0, &selected_model, 1, &selected_driver, -1);
	
		dbus_error_init (&error);
		gconf_set_printer_driver (dlg->priv->printer_make, dlg->priv->printer_model, selected_make, selected_model, selected_driver);
		dbus_set_printer_driver (selected_make, selected_model, dlg->priv->printer_udi, dlg->priv->printer_name, &error);

		if (dbus_error_is_set (&error)) {
			GtkWidget *error_dialog;

			error_dialog = gtk_message_dialog_new (NULL,
                                                               GTK_DIALOG_NO_SEPARATOR | GTK_DIALOG_MODAL,
                                                               GTK_MESSAGE_ERROR,
                                                               GTK_BUTTONS_CLOSE,
                                                               "%s", error.message);
			gtk_dialog_run (GTK_DIALOG (error_dialog));

			dbus_error_free (&error);
		}

		g_free (selected_driver);
		g_free (selected_model);
	}

	g_free (selected_make);
	gtk_widget_destroy (GTK_WIDGET (dlg));
}

static void
on_cancel_button_clicked (GtkButton *button,
                          gpointer user_data)
{
	ECDriverPromptDialog *dlg;
	dlg = EC_DRIVER_PROMPT_DIALOG (user_data);

	rb_debug ("cancel button clicked");
	
	gtk_widget_destroy (GTK_WIDGET (dlg));
}

void
ec_driver_prompt_dialog_set_and_show (ECDriverPromptDialog *dlg,
				      const gchar *make,
				      const gchar *model,
				      const gchar *printer_udi,
				      const gchar *printer_name)
{
	gchar *new_label_text;
	const gchar *info_label_text;

	dlg->priv->printer_udi = g_strdup (printer_udi);
	dlg->priv->printer_name = g_strdup (printer_name);
	dlg->priv->printer_make = g_strdup (make);
	dlg->priv->printer_model = g_strdup (model);

	info_label_text = gtk_label_get_label (GTK_LABEL (dlg->priv->info_label));

	if (info_label_text != NULL)
		new_label_text = g_strdup_printf (info_label_text, make, model);
	else
		new_label_text = g_strdup ("Error");

	gtk_label_set_label (GTK_LABEL (dlg->priv->info_label), new_label_text);
	g_free (new_label_text);

	gtk_widget_set_sensitive (dlg->priv->ok_button, FALSE);

	populate_dialog (dlg);

	gtk_widget_show_all (GTK_WIDGET (dlg));

}

gboolean
ec_driver_prompt_dialog_parse_message (ECDriverPromptDialog *dlg,
				     DBusMessage *msg)
{
	DBusError error;
	const gchar *make, *model, *printer_udi, *printer_name;


	dbus_error_init (&error);	

	if (!dbus_message_get_args (msg, &error,
                                    DBUS_TYPE_STRING, &make,
                                    DBUS_TYPE_STRING, &model,
                                    DBUS_TYPE_STRING, &printer_udi,
                                    DBUS_TYPE_STRING, &printer_name,
                                    DBUS_TYPE_INVALID))
	{
		g_error ("Error getting message aguments %s\n", error.message);
		return FALSE;
	}

	ec_driver_prompt_dialog_set_and_show (dlg, make, model, printer_udi, printer_name);

	return TRUE;
}


