/* -*- mode: c; style: linux -*-
 * Copyright (C) 2004 Red Hat, Inc 
 *
 * Written by Colin Walters <walters@redhat.com> 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <libgnome/gnome-i18n.h>
#include <libgnome/gnome-program.h>
#include <gtk/gtk.h>
#include <stdlib.h>
#include <libgnomecups/gnome-cups-printer.h>
#include <libgnomecups/gnome-cups-init.h>

enum {
	COLUMN_PRINTER = 0
};

static gboolean
printer_to_iter (GtkTreeModel *model, const char *target, GtkTreeIter *iter)
{
	if (!gtk_tree_model_get_iter_first (model, iter))
		return FALSE;
	do {
		GnomeCupsPrinter *printer;
		const char *name;
		gtk_tree_model_get (model, iter, COLUMN_PRINTER,
				    &printer, -1);
		name = gnome_cups_printer_get_name (printer);
		if (!strcmp (target, name))
			return TRUE;
	} while (gtk_tree_model_iter_next (model, iter));
	return FALSE;
}

static void
sync_default_printer (GtkTreeView *view)
{
	GtkTreeSelection *selection;
	char *default_printer;
	GtkTreeIter iter;

	selection = gtk_tree_view_get_selection (view);
	default_printer = gnome_cups_get_default ();

	if (printer_to_iter (gtk_tree_view_get_model (view),
			     default_printer,
			     &iter)) {
		gtk_tree_selection_select_iter (selection, &iter);
	} else {
		g_warning ("Couldn't find default printer %s in list!", default_printer);
	}
	g_free (default_printer);
}

static void
attributes_changed_cb (GnomeCupsPrinter *printer, gpointer data)
{
	GtkTreeModel *model;
	GtkTreeIter iter;
	const char *name;
	
	model = GTK_TREE_MODEL (data);

	name = gnome_cups_printer_get_name (printer);
	g_message ("printer %s attributes changed", name);
	
	if (printer_to_iter (model, name, &iter)) {
		GtkTreePath *path;
		path = gtk_tree_model_get_path (model, &iter);
		gtk_tree_model_row_changed (model, path, &iter);
		gtk_tree_path_free (path);
	} else {
		g_warning ("no row for printer %s?!", name);
	}
}

static void
add_printer (GtkListStore *model, const char *name)
{
	GtkTreeIter iter;
	GnomeCupsPrinter *printer;

	g_message ("adding printer %s", name);
	printer = gnome_cups_printer_get (name);
	g_return_if_fail (printer != NULL);

	g_signal_connect (printer, "attributes-changed",
			  G_CALLBACK (attributes_changed_cb),
			  model);
		
	gtk_list_store_append (model, &iter);
	gtk_list_store_set (model, &iter, COLUMN_PRINTER,
			    printer, -1);
}

static void
printer_added_cb (const char *name, gpointer user_data)
{
	GtkTreeView *view = GTK_TREE_VIEW (user_data);
	add_printer (GTK_LIST_STORE (gtk_tree_view_get_model (view)), name);
}

static void
printer_removed_cb (const char *name, gpointer user_data)
{
	GtkTreeView *view = GTK_TREE_VIEW (user_data);
	GtkTreeModel *model = gtk_tree_view_get_model (view);
	GtkListStore *store = GTK_LIST_STORE (model);
	GtkTreeIter iter;

	if (!printer_to_iter (model, name, &iter)) {
		g_warning ("unknown printer %s in printer_removed_cb", name);
		return;
	}
	
	gtk_list_store_remove (store, &iter);
}

static void
insert_initial_printers (GtkTreeView *view, GtkListStore *model)
{
	GList *printers, *printer;

	printers = gnome_cups_get_printers ();
	for (printer = printers; printer; printer = printer->next) {
		const char *name = printer->data;
		add_printer (model, name);
	}
	gnome_cups_printer_list_free (printers);
}

static void
dialog_response_cb (GtkDialog *dialog, guint response,
		    gpointer data)
{
	GtkTreeIter iter;
	GtkTreeView *view;
	GtkTreeSelection *selection;
	GtkTreeModel *model;
	const char *printer_name;
	GnomeCupsPrinter *printer;
	GError *error = NULL;

	if (response == GTK_RESPONSE_OK) {
		view = GTK_TREE_VIEW (data);
		selection = gtk_tree_view_get_selection (view);
		
		gtk_tree_selection_get_selected (selection, &model, &iter);

		gtk_tree_model_get (model, &iter, COLUMN_PRINTER,
				    &printer, -1);
		printer_name = gnome_cups_printer_get_name (printer);

		gnome_cups_printer_set_default (printer, &error);
		if (error && error->message)
			g_error ("Couldn't set default printer to %s: %s",
				 printer_name, error->message);
	}

	gtk_main_quit ();
}

static void 
printer_name_cell_data_func (GtkTreeViewColumn *tree_column,
			     GtkCellRenderer *cell,
			     GtkTreeModel *tree_model,
			     GtkTreeIter *iter,
			     gpointer data)
{
	GnomeCupsPrinter *printer;
	gtk_tree_model_get (tree_model, iter,
			    COLUMN_PRINTER,
			    &printer, -1);
	g_object_set (cell,
		      "text", gnome_cups_printer_get_name (printer),
		      NULL);
}

static void 
printer_location_cell_data_func (GtkTreeViewColumn *tree_column,
				 GtkCellRenderer *cell,
				 GtkTreeModel *tree_model,
				 GtkTreeIter *iter,
				 gpointer data)
{
	GnomeCupsPrinter *printer;
	gtk_tree_model_get (tree_model, iter,
			    COLUMN_PRINTER,
			    &printer, -1);
	g_object_set (cell,
		      "text", gnome_cups_printer_get_location (printer),
		      NULL);
}

int
main (int argc, char **argv) 
{
	GtkWidget *dialog;
	GtkWidget *scrollwin;
	GtkListStore *model;
	GtkTreeViewColumn *col;
	GtkWidget *vbox;
	GtkCellRenderer *rend;
	GtkWidget *view;

	bindtextdomain (GETTEXT_PACKAGE, EGGCUPS_LOCALEDIR);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);

	gtk_init (&argc, &argv);

	dialog = gtk_dialog_new_with_buttons (_("Default Printer"),
					      NULL,
					      GTK_DIALOG_MODAL,
					      GTK_STOCK_CLOSE,
					      GTK_RESPONSE_CLOSE,
					      _("_Set Default"),
					      GTK_RESPONSE_OK,
					      NULL);
	gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_OK);
	gtk_dialog_set_has_separator (GTK_DIALOG (dialog), FALSE);
	gtk_container_set_border_width (GTK_CONTAINER (dialog), 6);
	gtk_box_set_spacing (GTK_BOX (GTK_DIALOG (dialog)->vbox), 2);

	vbox = gtk_vbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox), vbox, TRUE,
			    TRUE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), 6);

	gnome_cups_init (NULL);

	model = gtk_list_store_new (1, GNOME_CUPS_TYPE_PRINTER);
	view = gtk_tree_view_new_with_model (GTK_TREE_MODEL (model));
	rend = gtk_cell_renderer_text_new ();
	col = gtk_tree_view_column_new_with_attributes (_("Printer"), 
							rend, 
							NULL);
	gtk_tree_view_column_set_cell_data_func (col, rend,
						 printer_name_cell_data_func,
						 NULL, NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (view), col);
	rend = gtk_cell_renderer_text_new ();
	col = gtk_tree_view_column_new_with_attributes (_("Location"), 
							rend, 
							NULL);
	gtk_tree_view_column_set_cell_data_func (col, rend,
						 printer_location_cell_data_func,
						 NULL, NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (view), col);
	scrollwin = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scrollwin),
					     GTK_SHADOW_IN);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrollwin),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	gtk_container_add (GTK_CONTAINER (scrollwin), view);
	gtk_box_pack_start (GTK_BOX (vbox), scrollwin, TRUE, TRUE, 0);

	/* Hmmm, hackish. */
	gtk_window_set_default_size (GTK_WINDOW (dialog), 320, 240);
	
	insert_initial_printers (GTK_TREE_VIEW (view), model);
	sync_default_printer (GTK_TREE_VIEW (view));

	gnome_cups_printer_new_printer_notify_add (printer_added_cb, view);
	gnome_cups_printer_printer_removed_notify_add (printer_removed_cb, view);
	
	g_signal_connect (dialog,
			  "response",
			  G_CALLBACK (dialog_response_cb),
			  view);
	gtk_widget_show_all (dialog);
	gtk_main ();
	
	exit (0);
}
