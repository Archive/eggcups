/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2004 Red Hat, Inc.
 *  Written by:
 *    Colin Walters <walters@redhat.com>
 *    Matthias Clasen <mclasen@redhat.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __EC_CUPS_MANAGER_H__
#define __EC_CUPS_MANAGER_H__

#include "config.h"
#include <glib-object.h>
#include <dbus/dbus.h>

G_BEGIN_DECLS

#define EC_TYPE_CUPS_MANAGER (ec_cups_manager_get_type ())
#define EC_CUPS_MANAGER(o) (G_TYPE_CHECK_INSTANCE_CAST ((o), EC_TYPE_CUPS_MANAGER, ECCupsManager))
#define EC_CUPS_MANAGER_CLASS(k) (G_TYPE_CHECK_CLASS_CAST((k), EC_TYPE_CUPS_MANAGER, ECCupsManagerClass))
#define EC_IS_CUPS_MANAGER(o) (G_TYPE_CHECK_INSTANCE_TYPE ((o), EC_TYPE_CUPS_MANAGER))
#define EC_IS_CUPS_MANAGER_CLASS(k) (G_TYPE_CHECK_CLASS_TYPE ((k), EC_TYPE_CUPS_MANAGER))
#define EC_CUPS_MANAGER_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), EC_TYPE_CUPS_MANAGER, ECCupsManagerClass)) 

typedef struct ECCupsManagerPrivate ECCupsManagerPrivate;

typedef struct
{
	GObject parent;

	ECCupsManagerPrivate *priv;
} ECCupsManager;

typedef struct
{
	GObjectClass parent;

} ECCupsManagerClass;

GType ec_cups_manager_get_type (void);

ECCupsManager * ec_cups_manager_new (DBusConnection *session);

G_END_DECLS

#endif /* __EC_CUPS_MANAGER_H__ */
