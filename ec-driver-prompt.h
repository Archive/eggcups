/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2004 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __EC_DRIVER_PROMPT__
#define __EC_DRIVER_PROMPT__

#include "config.h"
#include <dbus/dbus.h>
#include <glib.h>
#include <glib-object.h>

G_BEGIN_DECLS

#define EC_TYPE_DRIVER_PROMPT (ec_driver_prompt_get_type())
#define EC_DRIVER_PROMPT(object) (G_TYPE_CHECK_INSTANCE_CAST((object), EC_TYPE_DRIVER_PROMPT, ECDriverPrompt))
#define EC_DRIVER_PROMPT_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass), EC_TYPE_DRIVER_PROMPT, ECDriverPromptClass))
#define EC_IS_DRIVER_PROMPT(object) (G_TYPE_CHECK_INSTANCE_TYPE((object), EC_TYPE_DRIVER_PROMPT))
#define EC_IS_DRIVER_PROMPT_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), EC_TYPE_DRIVER_PROMPT))
#define EC_DRIVER_PROMPT_GET_CLASS(object) (G_TYPE_INSTANCE_GET_CLASS((object), EC_TYPE_DRIVER_PROMPT, ECDriverPromptClass))

typedef struct ECDriverPromptPrivate ECDriverPromptPrivate;

typedef struct _ECDriverPrompt 
{
	GObject parent;

	ECDriverPromptPrivate *priv;
} ECDriverPrompt;

typedef struct _ECDriverPromptClass
{
	GObjectClass parent;
} ECDriverPromptClass;

GType ec_driver_prompt_get_type (void);

ECDriverPrompt *ec_driver_prompt_new (DBusConnection *bus);

void ec_driver_prompt_set_connection (ECDriverPrompt *dp, DBusConnection *bus);

G_END_DECLS
#endif
